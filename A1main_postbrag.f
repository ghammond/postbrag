*DECK POSTBRAG
      PROGRAM POSTBRAG
C***********************************************************************
C
C                             POSTBRAG:
C
C             A Post-Processor for Output Data from BRAGFLO
C
C                              Authors
C                              -------
C
C                         James D. Schreiber
C            SCIENCE APPLICATIONS INTERNATIONAL CORPORATION
C
C                          Jonathan S. Rath
C                    Applied Mechanics Division
C              NEW MEXICO ENGINEERING RESEARCH INSTITUTE
C
C
C                              Abstract
C                              --------
C
C      POSTBRAG creates an output  CAMDAT from the output of the BRAGFLO
C   code and the input CAMDAT.  The program appends ANALYSIS information
C   output from the BRAGFLO code to the input CAMDAT.  Specifically, for
C   each  timestep of  BRAGFLO  output,  POSTBRAG  writes  values to the
C   CAMDAT.  These values are written to the  'Analysis Results' section
C   of the CAMDAT:   TIME, HIFLAG, HISTORY variables, GLOBAL  variables,
C   and ELEMENT variables (Pressures, brine and gas saturations, x-, y-,
C   and z-direction Darcy velocities, etc.).
C
C                            Specifications
C                            --------------
C
C   Program POSTBRAG appends the computational database as follows:
C
C       (1) Echoes the existing database, i.e.,  through 'variable iden-
C           tification -- element block,  element attributes,  and mate-
C           rial property names,' and inserts a new QA record.
C
C       (2) Sets variable identification information by reading history,
C           global,  nodal, and  element variable numbers and names from
C           BRAGFLO output file and writing to 'variable identification'
C           section in computational database.
C
C       (3) For each  timestep at which these are output, reads analysis
C           results (ELEMENT & GLOBAL information) from BRAGFLO output:
C             a. GLOBAL variables (material balances).
C             b. HISTORY variables.
C             c. ELEMENT variables (pressures, saturations, etc.),
C                as specified in the BRAGFLO output.
C
C       (4) Writes analysis results to computational database.
C
C
C
C                           Update History
C                           --------------
C
C  Date      Version   Changes
C  --------  --------  -------------------------------------------------
!apg 09/xx/12    4.02  *** Migrate from Alpha OpenVMS to Solaris ***
!apg                   Modified 09/xx/12 by Amy Gilkey (see !apg comments)
!apg                   All real variables are now double precision.
!apg                   Dynamic memory arrays are now allocated,
!apg                    not reserved with CAMSUPES_LIB routines.
!apg                   Minor code cleanup, such as allowing Unix file names
!apg                    or using "trim" to print strings.
!apg                   Minor cosmetic changes may be visible in the output.
C  03/27/07  4.00A     Corrected size of CNVFAC (Amy Gilkey).
C                      4.01 modifications have been discarded for now.
C  04/09/99  4.01      Modified QABRAG to be backwards compatible: read
C                      both BRAGFLO & PREBRAG dates in either mm/dd/yy
C                      or mm/dd/yyyy format.
C  02/02/99  4.01      Modified QABRAG to read BRAGFLO run date in for-
C                      mat mm/dd/yyyy instead of mm/dd/yy.
C  02/01/96  4.00      Added ability to add PREBRAG QA records to output
C                      CAMDAT file (James E. Bean UNM/NMERI)
C  08/11/95  3.05ZO    Removed unreferenced variables in PREPRO
C  07/31/95  3.04ZO    Modified subroutine PREPRO for conversion to CMS
C                      File names are now defined through command files
C                      using logicals
C  06/22/94  3.03ZO    Changed version number to new format.
C  06/10/94  C-3.02ZO  In QABRAG, when reading  ASCII output file to get
C                      NUMFIL, corrected internal read to convert STRING
C                      (34:37) to NUMFIL.   In SIZES,  changed  internal
C                      write to  internal read to convert character data
C                      to NVAREL2 and NVARGL2.   In REDMAP, deleted sec-
C                      tion that  read ASCII output  history  variables,
C                      because ASCII output has NO history variables.
C                      Also, changed NVTIME from 10 to 0 in REDMAP.
C  05/19/94  C-3.01ZO  In RBRAGO, added a check on NHIEL;  if NHIEL = 0,
C                      there are no  history  variables based on element
C                      variables,  so  blank  record  should not be read
C                      from  binary  file,  as  was being done.  Changed
C                      version  designation  from  VV to ZO  to  reflect
C                      operation on  ALPHA OpenVMS operating system.
C  11/09/93  C-3.00VV  Switched  order of HISTORY variables  so time-re-
C                      lated variables come first instead of last.  Read
C                      these first  NVTIME  history variable labels from
C                      BRAGFLO output instead of setting in SETNAM. Read
C                      and save as  INFO  record date of current BRAGFLO
C                      revision.   Cleaned up  RBRAGO  history  variable
C                      input; eliminated local variables. Revised QABRAG
C                      to read new QA header from ASCII output file. For
C                      BRAGFLO versions >3.00VV.
C  10/26/93  C-2.10VV  Adapted to  1993 BRAGFLO (versions >2.30).  Major
C                      changes include addition of HISTORY variables and
C                      many  additional  and/or  new  ELEMENT variables.
C                      BRAGFLO binary output file structure revised; or-
C                      der of  output  improved  to simplify reading and
C                      sizing arrays in POSTBRAG.  Most global variables
C                      were eliminated.   Descriptions and units of his-
C                      tory, global, and element variables added to out-
C                      CAMDAT  file  as  information  records.  Variable
C                      names and  descriptions read  from BRAGFLO output
C                      instead of being hardwired in POSTBRAG.
C
C  08/05/93  C-2.00VV  Completely new version of POSTBRAG, developed for
C                      BRAGFLO_T.   Adapted to  CAMDAT_LIB,  CAMCON_LIB.
C                      Improved  dynamic  memory  logic/flow, changed to
C                      CAMDAT_LIB's  standard  dynamic  character*1 base
C                      array.
C
C  07/07/93  C-1.58VV  In ATTDXY, changed  dimension of CNVFAC from (50,
C                      50) to (100,100) and changed dimension check (1st
C                      executable statement) accordingly.
C
C  06/07/93  C-1.57VV  Added vbl 63  to output;  changes  made in OUTSET
C                      and UNTCNV.INC.  Changed number of filenames read
C                      from BRAGFLO output from 6 to 7 in QABRAG.
C
C  01/13/92  C-1.56VV  Adapted to BRAGFLOT,  thermal version of BRAGFLO;
C                      added 14 new element variables in OUTSET; changed
C                      NMAP to 62 in UNTCNV.INC;  deleted  reading NGVAR
C                      in RBRAGO at each time step (no longer written).
C
C  01/13/92  C-1.55VV  In SCADB1, commented out last read statement that
C                      reads NVARHI, etc., from CDB file; it was causing
C                      problems because that line is not written in MAT-
C                      SET & not always in ICSET; also, NVARHI, etc. are
C                      set to  zero in PROCDB  immediately after call to
C                      SCADB1.
C
C  09/14/92  C-1.54VV  In QABRAG, changed month format from I2 to I2.2.
C
C  09/11/92  C-1.53VV  In PROCDB, increment NQAREC before dynamic memory
C                      is allocated  and again after  READB1 is  called,
C                      because  READB1 overwrites NQAREC with old value;
C                      removed CQ from PROCDB argument list; deleted the
C                      array QAINFO & insert NEWQAREC directly into CQ.
C
C  09/09/92  C-1.52VV  Fixed NQAREC at end of PROCDB; needs to be incre-
C                      mented to count new QA record for  "BRAGFLO";  it
C                      is incremented again in QASSMB to count POSTBRAG.
C
C  09/02/92  C-1.51VV  Revised logic so QABRAG is called 1st in POSTBRAG
C                      instead of after call to REDMAP,  which precludes
C                      having to rewind file to get QA stuff from top of
C                      file;  done by eliminating use of dynamic  memory
C                      in QABRAG.  Deleted unused  arguments in calls to
C                      RBRAGO, ATTDXY, & GETATT.
C
C  08/07/92  C-1.50VV  Adapted to output from BRAGFLO  version C-2.00VV.
C                      Reads names and values of many additional  global
C                      variables.  Added new ATTDXY & GETATT to get four
C                      new attributes (DELX, DELY, DELZ, & GRIDVOL) from
C                      new BRAGFLO output file.
C
C  04/03/92  C-1.42VV  Added element variables 49-51 in subroutine
C                      OUTSET and and changed NMAP in INCLUDE file
C                      UNTCNV.INC to 51.  Eliminated subroutine ATTDXY
C                      and calls to it in the main program.
C
C  03/04/92  C-1.41VV  Fixed processing of layer (z-direction) numbering
C                      in READEL;   fixed  argument  mismatch in call to
C                      READB1;  deleted unused variables found by FLINT;
C                      deleted unused variables in common blocks:  ARRAY
C                      (MAXQ), FILEUN (IDBA), & GEOPAR (NSTEP, KPR)
C
C  02/27/92  C-1.40VV  Added global variables (mass balances) & numerous
C                      element vbles.   Revised reading of element vbles
C                      for 3-D mesh.  Adapted  to new ASCII output file.
C                      Numerous cosmetic changes.
C
C  11/12/91  C-1.30VV  Added  capability  to  read  fixed-length  binary
C                      records,  as   written  on   Alliant   (NBINF=2),
C                      including  array  RVARS.   Added memory for array
C                      RVARS.   Added  Q(LRVARS)  to  RBRAGO  call list.
C                      Added  prompt  to indicate use of  Alliant binary
C                      output.
C
C  07/18/91  C-1.20VV  Fully  converted  to  BRAGFLO.   Cleaned up units
C                      conversion processes.   Now require debug file to
C                      be written.   New  subroutine  OUTSET  initalizes
C                      variable  names  &  search  string.   Added  many
C                      element variable names.   Deleted call to SCANBO,
C                      which  is  no longer  used.   Deleted use of NWAT
C                      in calls to SETNAM & RBRAGO;  added  NMAP & array
C                      NAMES to argument lists of PROCBO & SETNAM.
C
C  07/10/91  C-1.11VV  Further conversions from POSTBOAST  to  POSTBRAG.
C                      Eliminated  option   NOT  to  write  debug  file.
C                      Changed MAPOUT array dimension to *;  added  NMAP
C                      NMAP to argument list; revised format for reading
C                      MAPOUT from ASCII output.
C
C  05/09/91  C-1.10VV  Further conversions from POSTBOAST  to  POSTBRAG.
C                      Completely  revamped,   using  new  output  files
C                      structure  (mainly QA & output options sections).
C                      Replaced   GENERIC   library   subroutines   with
C                      CAMSUPES  library   subroutines;    revised   run
C                      procedure.
C
C  02/26/91  C-1.04VV  Added NVEL;  fixed test for completion of reading
C                      all required element variables.
C
C  02/21/91  C-1.03VV  Added MAPOUT array.  Changed RDEL to REAL.  Added
C                      calls to  GETDIS.   Added  MAPOUT  array arg list
C                      in call to READEL.  Changed RDEL to REAL.   Added
C                      call to  REDMAP  &  output control code map array
C                      MAPOUT; changed RDEL to REAL.
C
C  02/08/91  C-1.02VV  Simplified BOAST output file type inquiry
C
C  02/07/91  C-1.01VV  Cleaned up use of binary file
C
C  11/05/90  C-1.00VV  Original version, still incomplete;  adapted from
C                      POSTBOAST.
C
C
C
C                        Program Flowchart
C                        -----------------
C
      INCLUDE 'flwchr.inc'
C
C
C                              Disclaimer
C                              ----------
C
C       This  computer  program was prepared as an account of work spon-
C  sored by an  agency of the  United  States  Government.   Neither the
C  United  States  Government  nor any agency  thereof, nor any of their
C  employees,  nor any of their  contractors,  subcontractors,  or their
C  employees,  makes any warranty,  express or implied,  or  assumes any
C  legal liability or responsibility for the  accuracy, completeness, or
C  usefulness of any information,  apparatus,  product, or  process dis-
C  closed, or represents that its use would not infringe privately owned
C  rights. Reference herein to any specific commercial product, process,
C  or service by trade name, trademark, manufacturer, or otherwise, does
C  not necessarily  constitute or imply its endorsement, recommendation,
C  favoring by the United  States  Government, any agency thereof or any
C  of their  contractors or  subcontractors.  The views and opinions ex-
C  pressed  herein do  not  necessarily  state  or reflect  those of the
C  United States Government, any agency thereof or any of their contrac-
C  tors or subcontractors.
C
C
C                      Assumptions and Limitations
C                      ---------------------------
C
C    Language used is ANSI X3.9-1978 FORTRAN 77 except that comments and
C    Hollerith strings use lowercase characters.  Integer and real vari-
C    able names  are explicitly typed.   Machine dependent coding exists
C    in subroutine QABRAG.
C
C
C                   Files Accessed by Program POSTBRAG
C                   ------------------------------------
C      Input:
C      (1) Existing CAMDAT
C      (2) BRAGFLO output file:
C          Either the  standard ASCII output,
C          the standard binary output file, or
C          the fixed-record-length binary output file.
C
C      Output:
C      (4) New CAMDAT
C      (5) Diagnostic/Debug file; contains diagnostic messages.
C
C
C                               Assumptions
C                               -----------
C
C      (1)  Values for one or more output  distributions  (such as pres-
C           sure, phase saturations, Darcy velocities, or mass balances)
C           exist.   This is  achieved by  setting values for particular
C           options on the card of the  input file for the  BRAGFLO run,
C           where  options control the printing of saturations and pres-
C           sure. See the input manual for BRAGFLO for more information.
C      (2)  Values occur in particular order.  See documentation in sub-
C           routines  REDMAP,  QABRAG,  and SIZES for the order in which
C           values must appear.
C      (3)  Units used in the BRAGFLO  output are indicated near the top
C           of the  output  file.   If  English  units  are read in from
C           BRAGFLO,  they are  converted to SI units in POSTBRAG before
C           being written to CAMDAT.
C      (4)  This program accommodates only single or  multiple timesteps
C           (transient calculations) in BRAGFLO output.
C
C                                    Notes
C                                    -----
C
C      The JCL that runs this program should either:
C
C      (1)  Put the file names on the command line in this order: CAMDAT
C           input  file name,  BRAGFLO  output  file name, CAMDAT output
C           file name, ASCII POSTBRAG output file name,
C
C           or else:
C
C      (2)  Assign (COPY) the  corresponding files to given logical file
C           names:
C
C           INPUT/
C           (i)   Existing   database  (IDB = 31,  logical  file  name =
C                 '.CDB').  Whether  this  database is regional, source,
C                 or local doesn't matter to this translator program.
C           (ii)  BRAGFLO  ASCII output file  (IBOSTO = 3,  logical file
C                 name = '.OUT').   This file is a result of running the
C                 aforementioned  BRAGFLO input file through the BRAGFLO
C                 model.
C           (iii) BRAGFLO binary output file  (IBOSTO = 3,  logical file
C                 name = '.BIN').  This file is produced when BRAGFLO is
C                 run,  being  preferred over  the standard ASCII output
C                 when large amounts  of  output  will  be generated, as
C                 when many time steps or a large  number of grid blocks
C                 are used.   Binary files written on  non-VAX  machines
C                 have a special  format that  allows  transfer to,  and
C                 direct use on, the VAX; indicated by NBINF = 2.
C
C           OUTPUT/
C           (iii) New/updated VERSION of the database (IDB = 41, logical
C                 file name = 'BRAGFLO.CDB').
C           (iv)  Diagnostics/Debug   output file  (NOUTFL = 7,  logical
C                 file name = 'POSTBRAG.DBG').
C
C
C                           Types of data sets
C                           ------------------
C
C  Input/
C    BINARY: in units 'IDB','IBOSTO'(optionally)
C    ASCII:  in unit  'IBOSTO'
C
C  Output/
C    BINARY: in unit  'ODB'
C    ASCII:  in unit  'NOUTFL'
C
C                              Files Used
C                              ----------
C
C  filename                    units     description
C  --------                    -----     -----------
C  (CAMDAT_SXX_[i]_RYYY.CDB)    31       Input CAMDAT
C  (BRAGFLO_SXX_[i].OUT)         3       BRAGFLO ASCII output file
C  (BRAGFLO_SXX_[i].BIN)         3       BRAGFLO binary output file
C  (BRAGFLO_SXX_[i]_RYYY.CDB)   41       Output CAMDAT
C  (POSTBRAG.DBG)                7       ASCII diagnostics file
C
C  [i] - is Mesh Type
C  XX  - is Succinct Scenario Number
C  YYY - is LHS Run (Sample) Number
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'untcnv.inc'
      INCLUDE 'brag.inc'
      INTEGER IERR
      INTEGER IDUM
      LOGICAL HIFLAG0  !apg was REAL
      DOUBLE PRECISION CPUSEC, TIME0  !apg REAL
      CHARACTER*(132) FBRA, FIDB, FODB

      CHARACTER*80, ALLOCATABLE :: FILNAM(:)
      CHARACTER*8, ALLOCATABLE :: QAINFO(:,:)
      CHARACTER*80, ALLOCATABLE :: INFO(:)
      INTEGER, ALLOCATABLE :: NUMELB(:)
      INTEGER, ALLOCATABLE :: NATRIB(:)
      CHARACTER*8, ALLOCATABLE :: NAMATR(:)
      LOGICAL, ALLOCATABLE :: IASATR(:)
      DOUBLE PRECISION, ALLOCATABLE :: ATTRIB(:)
      INTEGER, ALLOCATABLE :: IEBLOC(:)

      LOGICAL LQ(1)
      INTEGER IQ(1)
      REAL Q(1)  !apg REAL
      CHARACTER*(1)   C(1)
      EQUIVALENCE( IQ, LQ, Q )

      EXTERNAL BLOCK
C
C<><><><><><><><><><><>
C...Begin Procedures...
C<><><><><><><><><><><>
C
C     ...Pre-process the program execution options and I/O files
      CALL PREPRO( FIDB, FBRA, FODB )

C     ...Initialize DYNAMIC base arrays
      CALL MDINIT( Q )
      CALL MCINIT( C )
!apg      call mdfill (1.0)  !apg init real arrays
!apg      call mcfill (' ')  !apg init char arrays
      CALL DBSETUP( Q, C, IERR )
      IF(IERR.NE.0)CALL QAABORT('(POSTBRAG) - Setting up DYNAMIC base'//
     $                          ' arrays ***')

C     ...Open input and output CAMDAT files
      CALL DBIOPEN( IDB, FIDB, IDUM, IDUM, IERR )
      IF(IERR.NE.0)CALL QAABORT('(POSTBRAG) - Opening input CAMDAT ***')
      CALL DBOOPEN( ODB, IDB, IDUM, FODB, IERR )
      IF(IERR.NE.0)CALL QAABORT('(POSTBRAG) - Opening output '//
     $                          'CAMDAT file **')

C     ...Set debugging/diagnostics file unit
      CALL DBERRUNI( NOUTFL )

C     ...Read input CAMDAT file and get array sizes  !apg move from PROCDB
      CALL DBISIZES( IDB,     NQAREC,  NXINFO,  NDIM,   NUMNOD,
     1               NELBLK,  NUMEL,   NX,      NY,     NZ,
     2               MAXNENO, NUQATR,  NUQPRP,  NUMNPS, LNPSNL,
     3               NUMESS,  LESSEL,  LESSNL,  IERR )
      IF(IERR.NE.0)CALL QAABORT('(PROCDB) - reading "HEADER" size '//
     $                          ' parameters and variables ***')
      CALL DBINVAR( IDB, NVARHI, NVARGL, NVARND, NVAREL, IERR )
      IF(IERR.NE.0)CALL QAABORT(
     $'(PROCDB) - reading number of ANALYSIS variables ***')

C     ...Add an extra attribute for GRIDVOLume  !apg move from PROCDB
      NUQATR2 = NUQATR+1

C     ...Reserve dynamic arrays  !apg move from PROCDB
      ALLOCATE (QAINFO(4,NQAREC+3))
      QAINFO = ' '  !apg
      ALLOCATE (INFO(NXINFO))
      INFO = ' '  !apg
      ALLOCATE (NUMELB(NELBLK))
      ALLOCATE (NATRIB(NELBLK))
      ALLOCATE (NAMATR(NUQATR2))
      NAMATR = ' '  !apg
      ALLOCATE (IASATR(NUQATR2*NELBLK))
      ALLOCATE (ATTRIB(NUQATR2*NUMEL))
      ATTRIB = 1.0  !apg
      ALLOCATE (IEBLOC(NUMEL*2))

C     ...Process input CAMDAT
      CALL PROCDB( QAINFO, INFO, 
     $             NUMELB, NATRIB, NAMATR, IASATR, ATTRIB, IEBLOC,
     $             TIME0,  HIFLAG0 )
!apg      IF(IERR.NE.0)CALL QAABORT('(POSTBRAG) - Closing input CAMDAT ***')
      CALL STATUS('PROCDB')

C     ...Open the BRAGFLO output file
      IF( NBINF.EQ.0 )THEN
C        ...Output file is ASCII
         CALL FILOPEN(IBOSTO,'INPUT','FORM',FBRA,IERR)
         IF(IERR.NE.0)CALL QAABORT(
     $   '(POSTBRAG) - Opening ASCII BRAGFLO file ***')
      ELSEIF( NBINF.GE.1 )THEN
C        ...Output file is binary
         CALL FILOPEN(IBOSTO,'INPUT','UNFORMATTED',FBRA,IERR)
         IF(IERR.NE.0)CALL QAABORT(
     $   '(POSTBRAG) - Opening Binary BRAGFLO file ***')
      ENDIF

C     ...Get the PREBRAG and BRAGFLO QA information
      CALL QABRAG( QAINFO )

C     ...Get the POSTBRAG QA information
      CALL QAMAKREC( NQAREC, QAINFO )

C     ...Write new QA records to CAMDAT file
C     ...PREBRAG QA Records (write only if they were found in the
C        output results file from BRAGFLO)

      IF(LQAPB)THEN
        CALL DBOQAREC( ODB, 'ADD', QAINFO(1,NQAREC-2), IERR )
        IF(IERR.NE.0)CALL QAABORT('(POSTBRAG) - Writing PREBRAG'//
     $  ' QA line to output CAMDAT ***')
      ENDIF

C     ...BRAGFLO QA Records
      CALL DBOQAREC( ODB, 'ADD', QAINFO(1,NQAREC-1), IERR )
      IF(IERR.NE.0)CALL QAABORT('(POSTBRAG) - Writing BRAGFLO'//
     $' QA line to output CAMDAT ***')

C     ...POSTBRAG QA Records
      CALL DBOQAREC( ODB, 'ADD', QAINFO(1,NQAREC), IERR )
      IF(IERR.NE.0)CALL QAABORT('(POSTBRAG) - Writing POSTBRAG'//
     $' QA line to output CAMDAT ***')
      CALL STATUS('QABRAG')

C     ...Get size info and file names from BRAGFLO output
      ALLOCATE (FILNAM(MAXFIL))
      FILNAM = ' '  !apg
      CALL SIZES( FILNAM )

C     ...Process the BRAGFLO results
      CALL PROCBO( FILNAM, INFO, 
     $             NUMELB, NATRIB, NAMATR, IASATR, ATTRIB, IEBLOC,
     $             TIME0,  HIFLAG0 )
      CALL STATUS('PROCBO')
      CLOSE (IBOSTO)

C     ...Complete ending procedures
      CALL DBPTITLE(ODB,NOUTFL,HEADER)
      CALL DBPVERS(ODB,NOUTFL)
      CALL DBPSIZES(ODB,NOUTFL)
      CALL DBPQAREC(ODB,NOUTFL,NQAREC,QAINFO)
      CALL DBICLOSE(IDB,IERR)
      IF(IERR.NE.0)CALL QAABORT('(POSTBRAG) - Closing input CAMDAT ***')
      CALL DBOCLOSE(ODB,IERR)
      IF(IERR.NE.0)CALL QAABORT('(POSTBRAG) - Closing output CAMDAT **')
      CALL QACPUS(NOUTFL,CPUSEC)
      CALL QAPAGE(NOUTFL,'END')
      CLOSE (NOUTFL)

      STOP 'POSTBRAG Normal Completion'
C
C---------------------------------------------------------------------
C**** End of PROGRAM POSTBRAG ****
C---------------------------------------------------------------------
C
      END
