### Makefile for POSTBRAG ###

# Name of executable
EXE = postbrag

# Source files
SRCf = \
 A1main_postbrag.f \
 attdxy.f          \
 block.f           \
 getatt.f          \
 getdis.f          \
 gieblo.f          \
 infrec.f          \
 prepro.f          \
 procbo.f          \
 procdb.f          \
 qabrag.f          \
 rbrago.f          \
 rctim0.f          \
 readel.f          \
 reahea.f          \
 redmap.f          \
 setnam.f          \
 sizes.f           \
 status.f          \
 wrcana.f          \
 wrchea.f         

# Commands and options for compile and link
FC = f95
FFLAGS = -O3 -m64 -finit-local-zero -fdefault-double-8 -fdefault-real-8 -fdefault-integer-8 -fno-aggressive-loop-optimizations
LD = f95
LDFLAGS = -O3 -m64 -finit-local-zero -fdefault-double-8 -fdefault-real-8 -fdefault-integer-8 -fno-aggressive-loop-optimizations

# Libraries to be linked in
LIBPATH = -L./
LIBS = -lcamdat -lcamcon -lcamsupes

# Object files (see compile rules below)
OBJf = $(SRCf:.f=.o)
OBJS = $(OBJf)

# For "make all"
all:: $(EXE)

# Link executable
$(EXE): $(OBJS)
	$(LD) -o $@ $(LDFLAGS)  $(OBJS)  $(LIBPATH) $(LIBS)

# Define compile rules for suffixes
.f.o:
	$(FC) -c $(FFLAGS) $*.f

