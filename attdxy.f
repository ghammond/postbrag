*DECK ATTDXY
      SUBROUTINE ATTDXY( NUMELB, NATRIB, IASATR, IEBLOC,
     $                   RVARS,  NAMATR, ATTRIB, CNVFAC )
C***********************************************************************
C***                                                                ****
C***  adds  ATTributes: Del_X, del_Y, del_z, and grid block volumes ****
C***        ___         _   _      _                                ****
C***                                                                ****
C***********************************************************************
C
C  PURPOSE:     Assigns attribute values corresponding to CAMDAT
C               DEL_X, DEL_Y, and DEL_Z from the BRAGFLO output.
C               Also, a new CAMDAT attribute is defined and assigned
C               values from the BRAGFLO output (grid block volume).
C
C  AUTHOR:      James D. Schreiber
C
C  UPDATED:     13-OCT-1993  --JDS: Adapted to POSTBRAG for BRAGFLO
C                                   version >2.30VV.  Deleted RDEL from
C                                   arg list & declarations, and from
C                                   use as arg in calls to GETATT.
C               02-AUG-1993  --JSR: Completely new version based
C                                   on modern CAMDATs with DEL_X,
C                                   DEL_Y, and DEL_Z already existing
C                                   as attributes.  Implemented with
C                                   modern CAMDAT_LIB and CAMCON_LIB
C                                   routines, and 1-Dimensional
C                                   arrays. For BRAGFLO_T.
C               07-JUL-1993  --JDS: Changed dimension of CNVFAC from
C                                   (50,50) to (100,100) and changed
C                                   dimension check (first statetment)
C                                   accordingly.
C               02-SEP-1992  --JDS: Deleted NUMELB array from arg list
C                                   & from GETATT calling arg list;
C                                   numerous cosmetic changes.
C               06-AUG-1992  --JDS: Completely rewritten version of an
C                                   old ATTDXY, to get DELX, etc. from
C                                   BRAGFLO C-2.00VV output.
C
C  CALLED BY:   PROCBO
C
C  CALLS:       GETATT
C               QAABORT
C
C  ARGUMENTS:
C   ENTRY/
C    --common blocks
C     /ATTNAM/ (from FORTRAN statement: INCLUDE 'attnam.inc')
C      NAMGRA = NAMes of GRid Attributes array (CHARACTER*8)
C
C     /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C      NX     = No. of elements along X-axis of mesh
C      NY     = No. of elements along Y-axis of mesh
C      NZ     = No. of elements along Z-axis of mesh
C      NUQATR = No. of unique attribute names
C      NUQATR2= No. of unique attribute names (w/additional att.)
C
C     /UNTCNV/ (from FORTRAN statement: INCLUDE 'untcnv.inc')
C      CFM3   = Converts cubic feet to cubic meters.
C      FTM    = Converts feet to meters.
C
C    --through subroutine call
C      NUMELB = No. of grid-blocks per material array (INTEGER)
C      IASATR = Attribute existence in material array (LOGICAL)
C      NATRIB = Number of attributes per material array (INTEGER)
C      IEBLOC = FD Global element transformation to CAMDAT element
C               number and CAMDAT material index array (INTEGER)
C      RVARS  = SCRATCH array (REAL)
C      CNVFAC = Units conversion factors array (REAL)
C
C   LOCAL/
C      NAX    = INTEGER variable (attribute number for del_x).
C      NAY    = INTEGER variable (attribute number for del_y).
C      NAZ    = INTEGER variable (attribute number for del_z).
C      NGRV   = INTEGER variable (attribute number for grid volume).
C
C   EXIT/
C    --through subroutine call
C      ATTRIB = Values of attributes (REAL)
C      NAMATR = Names of attributes (CHARACTER*8)
C
C***********************************************************************
C234567
      IMPLICIT  NONE
      INCLUDE 'attnam.inc'
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'untcnv.inc'
      INTEGER  I, INDX, J, K, NAX, NAY, NAZ, NGRV
      INTEGER  IEBLOC(*), NUMELB(*), NATRIB(*)
      DOUBLE PRECISION ATTRIB(*), CNVFAC(*), RVARS(*)  !apg REAL
      CHARACTER*(8) NAMATR(*)
      LOGICAL  IASATR(*)
C
C<><><><><><><><><><><>
C...Begin Procedures...
C<><><><><><><><><><><>
C
      NAX  = 0
      NAY  = 0
      NAZ  = 0
      NGRV = 0

C     ...Locate the array indicies corresponding to the
C        unique attribute names: DEL_X, DEL_Y, DEL_Z, & GRIDVOL
      DO 10 I=1,NUQATR2
         IF(NAMATR(I)(:8).EQ.NAMGRA(1)(:8))THEN
            NAX  = I
         ELSEIF(NAMATR(I)(:8).EQ.NAMGRA(2)(:8))THEN
            NAY  = I
         ELSEIF(NAMATR(I)(:8).EQ.NAMGRA(3)(:8))THEN
            NAZ  = I
         ELSEIF(NAMATR(I)(:8).EQ.NAMGRA(4)(:8))THEN
            NGRV = I
         ENDIF
  10  CONTINUE

      IF(NAX.EQ.0)THEN
         WRITE(NOUTFL,*)' *** MISSING critical attribute: ',
     $                  NAMGRA(1)(:8),' in CAMDAT ***'
         CALL QAABORT('(ATTDXY) - Missing attribute '//
     $                NAMGRA(1)//' in CAMDAT ***')
      ELSEIF(NAY.EQ.0)THEN
         WRITE(NOUTFL,*)' *** MISSING critical attribute: ',
     $                  NAMGRA(2)(:8),' in CAMDAT ***'
         CALL QAABORT('(ATTDXY) - Missing attribute '//
     $                NAMGRA(2)//' in CAMDAT ***')
      ELSEIF(NAZ.EQ.0)THEN
         WRITE(NOUTFL,*)' *** MISSING critical attribute: ',
     $                  NAMGRA(3)(:8),' in CAMDAT ***'
         CALL QAABORT('(ATTDXY) - Missing attribute '//
     $                NAMGRA(3)//' in CAMDAT ***')
      ENDIF

      IF(NGRV.EQ.0)THEN
C        ...Set the last name attribute name, GRIDVOL
         NGRV                = NUQATR2
         NAMATR(NUQATR2)(:8) = NAMGRA(4)(:8)
C        ...Set the ATTRIBUTE-MATERIAL existence truth array
         DO 20 I=1,NELBLK
            IF(NUMELB(I).GT.0.AND.NATRIB(I).GT.0)THEN
               INDX         = (NUQATR2-1)*NELBLK+I
               IASATR(INDX) = .TRUE.
            ELSE
               INDX         = (NUQATR2-1)*NELBLK+I
               IASATR(INDX) = .FALSE.  !apg
            ENDIF
  20     CONTINUE
      ENDIF

C     ...Set the UNIT CONVERSIONS for attributes w.r.t.
C        GRID-BLOCK dimensions and volumes.
      DO 500 K=1, NELBLK
         IF(NUMELB(K).GT.0.AND.NATRIB(K).GT.0)THEN
C           ...Material has element connectivity
C           ...Get DEL_X
            DO 110 J=1,NUQATR2
               INDX = (J-1)*NELBLK+K
               IF(IASATR(INDX).AND.
     $            NAMATR(J)(:8).EQ.NAMGRA(1)(:8))THEN
                  IF(BUNITS(:2).EQ.'SI')THEN
                     CNVFAC(INDX) = 1.0
                  ELSEIF(BUNITS(:7).EQ.'ENGLISH')THEN
                     CNVFAC(INDX) = FTM
                  ENDIF
                  GOTO 120
               ENDIF
 110        CONTINUE
 120        CONTINUE

C           ...Get DEL_Y
            DO 210 J=1,NUQATR2
               INDX = (J-1)*NELBLK+K
               IF(IASATR(INDX).AND.
     $            NAMATR(J)(:8).EQ.NAMGRA(2)(:8))THEN
                  IF(BUNITS(:2).EQ.'SI')THEN
                     CNVFAC(INDX) = 1.0
                  ELSEIF(BUNITS(:7).EQ.'ENGLISH')THEN
                     CNVFAC(INDX) = FTM
                  ENDIF
                  GOTO 220
               ENDIF
  210       CONTINUE
  220       CONTINUE

C           ....Get DEL_Z
            DO 310 J=1,NUQATR2
               INDX = (J-1)*NELBLK+K
               IF(IASATR(INDX).AND.
     $            NAMATR(J)(:8).EQ.NAMGRA(3)(:8))THEN
                  IF(BUNITS(:2).EQ.'SI')THEN
                     CNVFAC(INDX) = 1.0
                  ELSEIF(BUNITS(:7).EQ.'ENGLISH')THEN
                     CNVFAC(INDX) = FTM
                  ENDIF
                  GOTO 320
               ENDIF
  310       CONTINUE
  320       CONTINUE

C           ...Get GRIDVOLume
            DO 410 J=1,NUQATR2
               INDX = (J-1)*NELBLK+K
               IF(IASATR(INDX).AND.
     $            NAMATR(J)(:8).EQ.NAMGRA(4)(:8))THEN
                  IF(BUNITS(:2).EQ.'SI')THEN
                     CNVFAC(INDX) = 1.0
                  ELSEIF(BUNITS(:7).EQ.'ENGLISH')THEN
                     CNVFAC(INDX) = CFM3
                  ENDIF
                  GOTO 420
               ENDIF
 410        CONTINUE
 420        CONTINUE
         ENDIF
 500  CONTINUE

C     ...Get attributes DEL_X, DEL_Y, DEL_X, & GRIDVOL
      CALL GETATT( NAX,  IEBLOC, CNVFAC, RVARS, ATTRIB )
      CALL GETATT( NAY,  IEBLOC, CNVFAC, RVARS, ATTRIB )
      CALL GETATT( NAZ,  IEBLOC, CNVFAC, RVARS, ATTRIB )
      CALL GETATT( NGRV, IEBLOC, CNVFAC, RVARS, ATTRIB )

      RETURN
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE ATTDXY ****
C-----------------------------------------------------------------------
C
      END
