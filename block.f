*DECK BLOCK
      BLOCK DATA BLOCK
C***********************************************************************
C
C  PURPOSE:    Initializes variables that are in COMMON BLOCKS
C
C  AUTHOR:     Jonathan S. Rath
C
C  UPDATED:    22-JUN-1994  --JDS  Changed version number to new format.
C              10-JUN-1994  --JDS  Updated version number and revision
C                                  date following corrections to QABRAG
C                                  and SIZES.
C              19-MAY-1994  --JDS  Updated version number and revision
C                                  date.
C              26-OCT-1993  --JDS  Converted to POSTBRAG from
C                                  POSTBRAG_T; applicable to BRAGFLO
C                                  version >2.30VV.
C                                  Deleted units conversion factors
C                                  and NMAP & NGVMB; deleted the
C                                  "include" files: BRAG.INC
C                                  and UNTCNV.INC.
C              05-AUG-1993  --JSR  Original version for POSTBRAG_T.
C
C  ARGUMENTS:
C  ENTRY/
C      none
C
C  LOCAL/
C      none
C
C  EXIT/
C   --through common blocks:
C     /ATTNAM/ (from FORTRAN statement: INCLUDE 'attnam.inc')
C      NAMGRA = NAMes of GRid Attributes array (CHARACTER*8)
C
C     /CALEND/ (from FORTRAN statement: INCLUDE 'calend.inc')
C      MONTH  = VAX/VMS abbreivation of months in a year (CHARACTER*3)
C
C     /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C      IBOSTO = Device no. of BRAGFLO transfer file
C      IDB    = Device no. of input CAMDAT
C      ODB    = Device no. of output CAMDAT
C      NOUTFL = Device no. of output diagnostics/debug file
C
C     /TOOLS/  (from FORTRAN statement: INCLUDE 'tools.inc')
C      DEBUG  =  Logical for debugging dynamic memory allocation
C
C     /UNTCNV/ (from FORTRAN statement: INCLUDE 'untcnv.inc')
C      BUNITS = BRAGFLO type units (CHARACTER*8)
C             = 'ENGLISH' or
C             = 'SI'
C      FTM    = Conversion from ft to m (REAL)
C      CFM3   = Conversion from ft^3 to m^3 (REAL)
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'attnam.inc'
      INCLUDE 'calend.inc'
      INCLUDE 'fileun.inc'
      INCLUDE 'tools.inc'
      INCLUDE 'untcnv.inc'

C     ...Attribute Grid block variables (COMMON /ATTNAM)
      DATA NAMGRA/'DEL_X   ','DEL_Y   ','DEL_Z   ','GRIDVOL '/

C     ...VAX/VMS abbreivation of months in a year (COMMON /CALEND/)
      DATA MONTH /'JAN','FEB','MAR','APR','MAY','JUN',
     $            'JUL','AUG','SEP','OCT','NOV','DEC'/

C     ...File unit variables (COMMON /FILEUN/)
      DATA IBOSTO/3/IDB/31/ODB/41/NOUTFL/7/

C     ...Debugging variables (COMMON /TOOLS/)
      DATA DEBUG/.FALSE./

C     ...Unit conversion variables (COMMON /UNTCNV/)
      DATA FTM/3.0480000000000000E-01/, CFM3/2.8316846592000000E-02/
C
C-----------------------------------------------------------------------
C**** End of BLOCKDATA BLOCK ****
C-----------------------------------------------------------------------
C
      END
