*COMDECK BRAG
C***********************************************************************
C      /BRAG/ (from FORTRAN statement: INCLUDE 'BRAG.INC')
C       MAXFIL = Maximum number of files to EVER be used in BRAGFLO.
C                (this is a parameter, if more files are needed then
C                 increase it in the parmeter statement) 
C       NHIEL  = Total number of history variables that are based on
C                element variables (as opposed to CPU time, etc., that
C                are written at each time step and converted to history
C                vbls in POSTBRAG) (INTEGER)
C       NHIV   = Number of BRAGFLO output variables (element vbls) that
C                are written as HIstory Variables (INTEGER)
C       NUMFIL = Number of files used in BRAGFLO run (INTEGER)
C       NVTIME = Number of history variables read from same line as
C                time.  (Currently = 10).
C       NVTIMP = NVTIME + 1
C       CPUNAM = Name of computer that BRAGFLO was run on (CHARACTER)
C       REVDATE= Date of current BRAGFLO revision (CHARACTER)
C***********************************************************************
C234567
      INTEGER MAXFIL, NHIEL, NHIV, NUMFIL, NVTIME, NVTIMP
      PARAMETER (MAXFIL=10)
      CHARACTER CPUNAM*32, REVDATE*8
      COMMON /BRAG/ NHIEL, NHIV, NUMFIL, NVTIME, NVTIMP, CPUNAM,REVDATE
C***********************************************************************
