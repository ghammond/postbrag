*COMDECK FILEUN
C***********************************************************************
C     /FILEUN/ (from FORTRAN statement: INCLUDE 'FILEUN.INC')
C      IBOSTO = Device no. of BRAGFLO output file
C      IDB    = Device no. of input CAMDAT
C      ODB    = Device no. of output CAMDAT
C      NBINF  = Determines BRAGFLO output file type:
C             = 0;ASCII output file
C             = 1;BINARY output file, variable-length records
C             = 2;BINARY output file, fixed-length records
C      NOUTFL = Device no. of output diagnostics/debug file
C***********************************************************************
C234567
      INTEGER IBOSTO, IDB, ODB, NBINF, NOUTFL
      COMMON /FILEUN/ IBOSTO, IDB, ODB, NBINF, NOUTFL
C***********************************************************************
