*COMDECK FLWCHR
C
C  POSTBRAG-+-PREPRO-+-(QASETUP) 
C           |        |
C           |        +-(QABANNER) 
C           |        |
C           |        +-(FILCMDLIN) 
C           |        |
C           |        +-(FILDFNAM) 
C           |        |
C           |        +-(FILRDNAMS) 
C           |        |
C           |        +-(FILWRNAMS) 
C           |        |
C           |        +-(QAABORT) 
C           |        |
C           |        +-(FILOPEN) 
C           |        |
C           |        +-(IQAERRUNI) 
C           |        |
C           |        +-(QAPAGE) 
C           |        |
C           |        +-(QADOEDIS) 
C           |
C           +-(MDINIT) 
C           |
C           +-(MCINIT) 
C           |
C           +-(DBSETUP) 
C           |
C           +-(QAABORT) 
C           |
C           +-(DBIOPEN) 
C           |
C           +-(DBOOPEN) 
C           |
C           +-(DBERRUNI) 
C           |
C           +-PROCDB-+-(DBISIZES) 
C           |        |
C           |        +-(QAABORT) 
C           |        |
C           |        +-(DBINVAR) 
C           |        |
C           |        +-ADDMEM (1)--(QAABORT) 
C           |        |
C           |        +-DYNMEM (2)--MEMADJ-+-(QAABORT) 
C           |        |                    |
C           |        |                    +-(MDGET) 
C           |        |                    |
C           |        |                    +-(MDFILL) 
C           |        |                    |
C           |        |                    +-(MDRSRV) 
C           |        |                    |
C           |        |                    +-(MDDEBG) 
C           |        |                    |
C           |        |                    |
C           |        |                    +-(MDSTAT) 
C           |        |                    |
C           |        |                    +-(MDLIST) 
C           |        |                    |
C           |        |                    +-(MDEROR) 
C           |        |
C           |        +-ZERODA
C           |        |
C           |        +-DYNMEMC   (3)--MEMADJC-+-(QAABORT) 
C           |        |                      |
C           |        |                      +-(MCGET) 
C           |        |                      |
C           |        |                      +-(MCFILL) 
C           |        |                      |
C           |        |                      +-(MCRSRV) 
C           |        |                      |
C           |        |                      +-(MCDEBG) 
C           |        |                      |
C           |        |                      +-(MCSTAT) 
C           |        |                      |
C           |        |                      +-(MCLIST) 
C           |        |                      |
C           |        |                      +-(MCEROR) 
C           |        |
C           |        +-REAHEA-+-(DBITITLE) 
C           |        |        |
C           |        |        +-(QAABORT) 
C           |        |        |
C           |        |        +-(DBIXYZ) 
C           |        |        |
C           |        |        +-(DBIMAP) 
C           |        |        |
C           |        |        +-(DBIELBLK) 
C           |        |        |
C           |        |        +-(DBILINK) 
C           |        |        |
C           |        |        +-(DBIPRNAM) 
C           |        |        |
C           |        |        +-(DBIPROP) 
C           |        |        |
C           |        |        +-(DBIATNAM) 
C           |        |        |
C           |        |        +-(DBIATTR) 
C           |        |        |
C           |        |        +-(DBIINFO) 
C           |        |        |
C           |        |        +-(DBIQAREC) 
C           |        |        |
C           |        |        +-(DBINSTEP) 
C           |        |
C           |        +-GIEBLO--(QAABORT) 
C           |        |
C           |        +-RCTIM0-+-(DBITIME) 
C           |                 |
C           |                 +-(QAABORT) 
C           |
C           |
C           +-STATUS
C           |
C           +-(FILOPEN) 
C           |
C           +-QABRAG-+-(QAMAXERR) 
C           |        |
C           |        +-(QAABORT) 
C           |
C           +-(QAMAKREC) 
C           |
C           +-(DBOQAREC) 
C           |
C           +-SIZES--(QAABORT) 
C           |
C           +-ADDMEM1>ADDMEM see 1
C           |
C           +-DYNMEM see 2
C           |
C           +-ZERODA
C           |
C           +-DYNMEMC   see 3
C           |
C           +-PROCBO-+-REDMAP--(QAABORT) 
C           |        |
C           |        +-SETNAM
C           |        |
C           |        +-ATTDXY-+-(QAABORT) 
C           |        |        |
C           |        |        +-GETATT--(QAABORT) 
C           |        |
C           |        +-INFREC--(QAABORT) 
C           |        |
C           |        +-WRCHEA-+-(QAABORT) 
C           |        |        |
C           |        |        +-(DBOINFO) 
C           |        |        |
C           |        |        +-(DBOTITLE) 
C           |        |        |
C           |        |        +-(DBOATTR) 
C           |        |        |
C           |        |        +-(DBOHEAD) 
C           |        |        |
C           |        |        +-(DBOVRNAM) 
C           |        |
C           |        +-RBRAGO--GETDIS-+-(QAABORT) 
C           |        |                |
C           |        |                +-READEL--(QAABORT) 
C           |        |
C           |        +-WRCANA-+-(DBOTIME) 
C           |        |        |
C           |        |        +-(DBOVAR) 
C           |        |        |
C           |        |        +-(QAABORT) 
C           |        |        |
C           |        |        +-(DBOSTEP) 
C           |        |
C           |        |
C           |        +-(QAABORT) 
C           |
C           +-(DBPTITLE) 
C           |
C           +-(DBPVERS) 
C           |
C           +-(DBPSIZES) 
C           |
C           +-(DBPQAREC) 
C           |
C           +-(DBICLOSE) 
C           |
C           +-(DBOCLOSE) 
C           |
C           +-(QACPUS) 
C           |
C           +-(QAPAGE) 
C    
