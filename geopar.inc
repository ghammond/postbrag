*COMDECK GEOPAR
C***********************************************************************
C    /GEOPAR/ (from FORTRAN statement: INCLUDE 'GEOPAR.INC')
C     HEADER = Character description of problem, etc... (CHARACTER*80)
C     LESSEL = List of Element Side sets concatenated Element List
C     LESSNL = List of Element Side sets concatenated Nodes associated
C              with each element side set Node List
C     LNPSNL = List of Nodal Point set concatenated Nodes List 
C     LQAPB  = Logical varible that is true when PREBRAG QA records
C              were found in the BRAGFLO binary or ASCII output results file
C     MAXNENO= Max. no. of nodes of any element
C     NDIM   = Number of physical DIMensions
C     NELBLK = Number of ELement BLocKs in mesh
C     NEXINFO= Number of BRAGFLO "extra" information records (INTEGER)
C            = +1 for CPUNAME
C              +1 for CAMDAT units used
C              +3 for extra file name header records
C              +NVAREL2
C              +NVARGL2
C              +NVARHI2
C            = (4+NVAREL2+NVARGL2+NVARHI2)
C     NX     = Number of elements along X-axis of mesh
C     NY     = Number of elements along Y-axis of mesh
C     NZ     = Number of elements along Z-axis of mesh
C     NXY    = NX*NY
C     NQAREC = Number of QA records
C     NSTEP  = Number of time steps
C     NUMEL  = NUmber of ELeMents in mesh
C     NUMESS = NUMber of Element Side set in mesh 
C     NUMNOD = NUMber of NODes in mesh
C     NUMNPS = NUMber of Nodal Point set in mesh
C     NUQATR = No. of UniQue AtTRibute names
C     NUQATR2= No. of UniQue AtTRibute names after adding additional
C              POSTBRAG attributes.
C     NUQPRP = No. of UniQue PRoPerty names
C     NVAREL = No. of Element variables
C     NVARGL = "      Global  "
C     NVARHI = "      History "
C     NVARND = "      Nodal   "
C     NXINFO = Number of optional additional information records
C     NXINFO2= Number of optional additional information records +
C              additional records required by POSTBRAG.
C     UNITSC = Unit system used throught CAMDAT (CHARACTER*8)
C***********************************************************************
C234567
      CHARACTER*(8)  UNITSC
      CHARACTER*(80) HEADER
      LOGICAL LQAPB
      INTEGER 
     $LESSEL,  LESSNL,  LNPSNL,  MAXNENO, NDIM,    NELBLK,  NEXINFO,
     $NQAREC,  NSTEP,   NUMEL,   NUMESS,  NUMNOD,  NUMNPS,  NUQATR,  
     $NUQATR2, NUQPRP,  NVARHI,  NVARGL,  NVARND,  NVAREL,  NXINFO, 
     $NXINFO2, NX,      NY,      NZ,      NXY
      COMMON /GEOPAR/ 
     $LESSEL,  LESSNL,  LNPSNL,  MAXNENO, NDIM,    NELBLK,  NEXINFO,
     $NQAREC,  NSTEP,   NUMEL,   NUMESS,  NUMNOD,  NUMNPS,  NUQATR,  
     $NUQATR2, NUQPRP,  NVARHI,  NVARGL,  NVARND,  NVAREL,  NXINFO, 
     $NXINFO2, NX,      NY,      NZ,      NXY,     HEADER,  UNITSC,
     $LQAPB
C***********************************************************************

