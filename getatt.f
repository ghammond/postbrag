*DECK GETATT
      SUBROUTINE GETATT( IDXATT, IEBLOC, CNVFAC, RVARS, ATTRIB )
C***********************************************************************
C***                                                                ****
C***       G_E_T  A_T_T_ributes  from  the  BRAGLFO  output         ****
C***       - - -  - - -                                             ****
C***********************************************************************
C
C  PURPOSE:     Reads the BRAGFLO output data relative to DEL_X, DEL_Y,
C               DEL_Z, and GRIDVOL and assigns these values to the
C               POSTBRAG/CAMDAT attribute values array.
C
C  AUTHOR:      James D. Schreiber
C
C  UPDATED:     13-OCT-1993  --JDS: Changes in comments adapting to
C                                   POSTBRAG from POSTBRAG_T.  Deleted
C                                   RDEL from arg list & declarations;
C                                   not used following JSR's revisions.
C               02-AUG-1993  --JSR: Adapted to CAMDAT_LIB, adapted
C                                   for dynamic memory 1-D arrays, etc.
C               02-SEP-1992  --JDS: Cosmetic changes.
C               06-AUG-1992  --JDS: Original version,
C                                   adapted from GETDIS.
C
C  CALLED BY:   ATTDXY
C
C  CALLS:       QAABORT
C
C  ARGUMENTS:
C   ENTRY/
C    --common blocks
C     /FILEUN/ (include 'fileun.inc')
C      IBOSTO = File unit of BRAGFLO output (POSTBRAG input) file.
C      NBINF  = Flag indicating type of BRAGFLO output file.
C      NOUTFL = File unit of POSTBRAG diagnostics/debug output file.
C
C     /GEOPAR/ (include 'geopar.inc')
C      NELBLK = Number of materials in CAMDAT (INTEGER)
C      NUMEL  = Number of elements in mesh = NX*NY*NZ.
C      NUQATR2= Number of unique attributes (INTEGER)
C      NX     = Number of elements in x-direction.
C      NXY    = NX*NY.
C      NY     = Number of elements in y-direction.
C      NZ     = Number of elements in z-direction.
C
C    --through subroutine call
C      IDXATT = Unique attribute index (INTEGER)
C      CNVFAC = Units conversion factors array (REAL)
C      IEBLOC = Global element number array.
C      RVARS  = REAL scratch array (memory allocated dynamically).
C
C   LOCAL/
C      NK     = Scratch INTEGER variable.
C
C   EXIT/
C    --through subroutine call
C      ATTRIB = CAMDAT attribute values array (REAL)
C
C***********************************************************************
C234567
      IMPLICIT  NONE
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'untcnv.inc'
      INTEGER I,      ICAMAT, ICAMEL, IDXATT, IEND,   II,    IPOS,
     $        IPOSC,  ISTART, JJ,     KK,     L1,     L2,    M,
     $        KL,     NK,     NJK,    NEL
      INTEGER IEBLOC(*)
      DOUBLE PRECISION ATTRIB(*), CNVFAC(*), RVARS(*)  !apg REAL
      CHARACTER*(132) CLINE
C
C<><><><><><><><><><><>
C...BEGIN PROCEDURES...
C<><><><><><><><><><><>
C
C     *** If output is BINARY, read the data now ***
C
      IF(NBINF .EQ. 1)THEN
C        ...BINARY BRAGFLO output; variable-length records
         READ(IBOSTO,END=110,ERR=100)(RVARS(I),I=1,NUMEL)
C
      ELSEIF(NBINF .EQ. 2)THEN
C        ...BINARY BRAGFLO output; fixed-length records
         DO 10 M=1,(NUMEL+127)/128
            L1 = (M-1)*128 + 1
            L2 = MIN(L1+127,NUMEL)
            READ(IBOSTO,END=110,ERR=100) (RVARS(II),II=L1,L2)
  10     CONTINUE

      ELSEIF(NBINF .EQ. 0)THEN
C        ...ASCII BRAGFLO output written
C        ...Read dashed line & title in ASCII file ("Grid Block...")
         READ(IBOSTO,'(A)',END=110,ERR=100) CLINE
         READ(IBOSTO,'(A)',END=110,ERR=100) CLINE
      ENDIF

C     ...Loop over layers (done for both binary & ASCII output)
      DO 90 KK=1,NZ
         IF(NBINF .EQ. 0)THEN
C           ...If ASCII output, read the line with "Layer #"
            READ(IBOSTO,'(A)',END=110,ERR=100) CLINE
C           ...Reverse the order -- layer NZ 1st in ASCII output
            KL = NZ - KK + 1
         ELSE
            KL = KK
         ENDIF
         NK = (KL-1)*NXY
         DO 80 JJ=1,NY
            IF(NBINF .EQ. 0)THEN
C              ...ASCII data must be read now
               ISTART = (KL-1)*NX*NY+(JJ-1)*NX+1
               IEND   = ISTART+NX-1
               READ(IBOSTO,*,END=110,ERR=100)
     $         M,(RVARS(II),II=ISTART,IEND)
            ENDIF

            NJK = (JJ-1)*NX + NK
C
            DO 70 II=1,NX
C              ...Get global element number for
C                 BRAGFLO grid-block (II,JJ,KK)
               NEL    = NJK + II
C              ...Get CAMDAT global elemnt number
               ICAMEL = IEBLOC(2*NEL-1)
C              ...Get CAMDAT material index
               ICAMAT = IEBLOC(2*NEL)
C              ...Get CAMDAT ATTRIB(*) array position
               IPOS   = (IDXATT-1)*NUMEL+ICAMEL
C              ...Get Conversion Factor array position for attributes
               IPOSC  = (IDXATT-1)*NELBLK+ICAMAT
C              ...Assign CAMDAT attribute grid-block data
C                 from BRAGFLO output & convert units
               ATTRIB(IPOS) = CNVFAC(IPOSC)*RVARS(NEL)
  70        CONTINUE
  80     CONTINUE
  90  CONTINUE
      RETURN

C     ...ERRORS detected
 100  WRITE(NOUTFL,*)
     $' *** ERROR reading BRAGFLO output file grid-block data ***'
      CALL QAABORT(
     $'(GETATT) - reading BRAGFLO output file grid-block data ***')

C     ...END-OF-FILE found
 110  IF(NBINF .EQ. 0)THEN
         WRITE(NOUTFL,*)' *** E-O-F ASCII read in GETATT ***'
         BACKSPACE (IBOSTO)
         READ(IBOSTO,'(A)') CLINE
         WRITE(NOUTFL,*)' *** Last ASCII line read in GETATT:'
         WRITE(NOUTFL,'(A)') CLINE
      ELSE
         WRITE (NOUTFL,*)' *** E-O-F binary read in GETATT ***'
      ENDIF
      CALL QAABORT('(GETATT) - Premature EOF in BRAGFLO output '//
     $' reading grid-block data ***')
      RETURN
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE GETATT ****
C-----------------------------------------------------------------------
C
      END
