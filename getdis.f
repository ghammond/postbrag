*DECK GETDIS
      SUBROUTINE GETDIS( DISNAM, NAMELV2, VALELV2, IEBLOC, KUIT,
     $                   CNVFAC, RVARS )
C***********************************************************************
C***                                                                 ***
C***           G_E_T_s  D_I_S_tributions  (Element Variables)        ***
C***           - - -    - - -                                        ***
C***********************************************************************
C
C  PURPOSE:     Reads the Element Variable distributions in BRAGFLO
C               output file and fills POSTBRAG output arrays.
C
C  AUTHOR:      James D. Schreiber
C
C  UPDATED:     07-SEP-1993  --JDS: Comments changed to reflect POSTBRAG
C                                   instead of POSTBRAG_T.  Deleted RDEL
C                                   from argument list and from use in
C                                   READEL since no longer used in READEL
C                                   to read in 3-D arrays.
C               02-AUG-1993  --JSR: Adapted to CAMDAT_LIB and CAMCON_LIB
C                                   routines and to POSTBRAG_T.
C               11-NOV-1991  --JDS: Added RVARS to argument list;
C                                   also added RVARS array in the
C                                   argument list in call to READEL.
C               12-JUL-1991  --JDS: Added UNTCNV.INC;
C                                   Deleted BUNITS from argument list
C                                   (it's now in UNTCNV.INC).
C               09-MAY-1991  --JDS: Converted from BOAST to BRAGFLO.
C               21-FEB-1991  --JDS: Original version.
C
C  CALLED BY:   RBRAGO
C
C  CALLS:       QAABORT
C
C  ARGUMENTS:
C   ENTRY/
C    --common blocks
C     /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C      NOUTFL = File unit of diagnostics/debug output file.
C
C     /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C      NUMEL  = Number of elemnts in mesh (INTEGER)
C      NELBLK = Number of Element Blocks (Regions) in mesh.
C      UNITSC = Units to be used in CAMDAT file.
C
C     /NEWANA/ (from FORTRAN statement: INCLUDE 'newana.inc')
C      NVAREL2= Number of NEW CAMDAT Element variables
C      NVARGL2= Number of NEW CAMDAT Global  variables
C      NVARHI2= Number of NEW CAMDAT History variables
C
C    --through subroutine call
C      DISNAM = BRAGFLO output element variable name (CHARACTER*10)
C      IEBLOC = Global element number array (INTEGER)
C      NAMELV2= Element Variable Names array (CHARACTER*8)
C      RVARS  = REAL scratch array (memory allocated dynamically).
C
C   LOCAL/
C      I      = DO loop control parameter.
C      IVAR   = Number of variable name in NAMELV or NAMGLV array.
C      J      = DO loop control parameter.
C      K      = DO loop control parameter.
C
C   EXIT/
C    --through subroutine call
C      KUIT   = End of file FLAG (INTEGER)
C             = 0; no EOF found
C             = 1; EOF found in subroutine READEL
C      VALELV2= CAMDAT Element Variable values array (REAL)
C
C***********************************************************************
C234567
      IMPLICIT  NONE
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'newana.inc'
      INCLUDE 'untcnv.inc'
      INTEGER I, IPT, IVAR, KUIT
      INTEGER IEBLOC(*)
      DOUBLE PRECISION CNVFAC  !apg REAL
      DOUBLE PRECISION RVARS(*), VALELV2(*)  !apg REAL
      CHARACTER*(8) DISNAM
      CHARACTER*(8) NAMELV2(*)
C
C<><><><><><><><><><><>
C...BEGIN PROCEDURES...
C<><><><><><><><><><><>
C
      IVAR = 0
      KUIT = 0
      DO 10 I=1,NVAREL2
         IF(NAMELV2(I)(:8).EQ.DISNAM(:8))THEN
            IVAR = I
            GOTO 20
         ENDIF
  10  CONTINUE

  20  CONTINUE
      IF(IVAR.EQ.0)THEN
         WRITE(NOUTFL,900)DISNAM(:8)
         DO 30 I=1,NVAREL2
            WRITE(NOUTFL,910)I,NAMELV2(I)(:8)
  30     CONTINUE
         CALL QAABORT('(GETDIS) - DISNAM not found in NAMELV2(*) ***')
      ENDIF

C     ...Read BRAGFLO element variables into CAMDAT VELELV2 array
      CALL READEL( IVAR, VALELV2, KUIT, IEBLOC, RVARS )
      IF(KUIT.EQ.1)THEN
C        ...E-O-F encountered in READEL
C        ...Error flag (KUIT) will be processed in RBRAGO routine
      ELSE
C        ...READEL successful!
C        ...Convert to proper units
         DO 40 I=1,NUMEL
            IPT          = (IVAR-1)*NUMEL+I
            VALELV2(IPT) = CNVFAC*VALELV2(IPT)
  40     CONTINUE
      ENDIF

      RETURN
C
C----------------  F O R M A T   S T A T E M E N T S  ------------------
C
 900  FORMAT(' *** DISNAM=',A,' not found in NAMELV2(*) array ***',/,
     $       '     NAMELV2(*) array dump follows:')
 910  FORMAT('     NAMELV2(',I5,')=',A)
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE GETDIS ****
C-----------------------------------------------------------------------
C
      END
