*DECK GIEBLO
      SUBROUTINE GIEBLO( MAP, NUMELB, IEBLOC )
C***********************************************************************
C***                                                                ****
C*** G_enerate I_nverse E_lement trans. + element B_L_O_cking array ****
C*** -         -        -                         - - -             ****
C***********************************************************************
C
C   PURPOSE:     Generates the global element number (FD or NETWORK)
C                to CAMDAT element number transformation array.  In
C                addition, the element blocking index (material region
C                index) is also stored.
C
C   AUTHOR:      Jonathan S. Rath
C
C   UPDATED:     16-SEP-1993  --JDS: No changes; now used in POSTBRAG
C                                    for BRAGFLO version >2.30VV.
C                23-JUL-1993  --JSR: First Ed. for POSTBRAG_T
C
C   CALLED BY:   PROCDB
C
C   CALLS:       QAABORT
C
C   ARGUMENTS:
C   ENTRY/
C     --common block
C      /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C       NOUTFL = File unit of diagnostics/debug output file
C
C      /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C       NUMEL  = NUmber of ELeMents in mesh
C       NELBLK = Number of ELement BLocKs in mesh
C
C     --through subroutine call
C       MAP    = Integer transformation array relating CAMDAT element
C                numbers to FD-grid element numbers.
C       NUMELB = No. of elements per element block INTEGER array
C
C   LOCAL/
C       none
C
C   EXIT/
C     --through subroutine call
C       IEBLOC = Global element/element block special CONNECTIVITY array
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INTEGER ICAMEL, IFDEL, J, K
      INTEGER IEBLOC(*), MAP(*), NUMELB(*)
C
C<><><><><><><><><><><>
C...Begin Procedures...
C<><><><><><><><><><><>
C
      ICAMEL  = 0
      IFDEL   = 0
      DO 20 K = 1, NELBLK
         DO 10 J = 1, NUMELB(K)
            ICAMEL = ICAMEL+1
            IFDEL  = MAP(ICAMEL)
            IF(ICAMEL.LE.0.OR.ICAMEL.GT.NUMEL)THEN
               WRITE(NOUTFL,900)ICAMEL,NUMEL
               CALL QAABORT('(GIEBLO) - CAMDAT element no. ***')
            ENDIF
            IF(IFDEL.LE.0.OR.IFDEL.GT.NUMEL)THEN
               WRITE(NOUTFL,910)IFDEL,NUMEL
               CALL QAABORT('(GIEBLO) - MAP array error ***')
            ENDIF
            IEBLOC(2*IFDEL-1) = ICAMEL
            IEBLOC(2*IFDEL  ) = K
  10     CONTINUE
  20  CONTINUE
      RETURN
C
C----------------  F O R M A T   S T A T E M E N T S  ------------------
C
 900  FORMAT(/1X,' *** ERROR counting "CAMDAT element numbers" ***',
     $       /1X,'     CAMDAT element numbers == summation i=1,nelblk',
     $      '(iecam=iecam$numelb(i))',
     $       /1X,'     ILLEGAL CAMDAT element no. = ',I12,
     $       /1X,'     No. of element in mesh/network (NUMEL) = ',I12)
 910  FORMAT(/1X,' *** ERROR counting "FD or NETWORK element no.s ***',
     $       /1X,'     Illegal or infected MAP(*) array detected',
     $       /1X,'     ILLEGAL FD or NETWORK element no. = ',I12,
     $       /1X,'     No. of element in mesh/network (NUMEL) = ',I12)
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE GIEBLO ****
C-----------------------------------------------------------------------
C
      END
