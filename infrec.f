*DECK INFREC
      SUBROUTINE INFREC( EXINFO,  NAMELV2, NAMGLV2,  NAMHIV2,  LABELEL,
     $                   LABELGL, LABELHI, LABUNTEL, LABUNTGL, LABUNTHI)
C***********************************************************************
C***                                                                ****
C***      Creates new I_N_F_ormation R_E_C_ords for CAMDAT file     ****
C***                  - - -          - - -                          ****
C***********************************************************************
C
C   PURPOSE:     Having read or otherwise established all variable info,
C                including names, descriptive labels, and units labels,
C                in REDMAP and SETNAM, this subroutine assembles this
C                information into information records that are saved in
C                the CAMDAT file.
C
C   AUTHOR:      James D. Schreiber
C
C   UPDATED:     09-NOV-1993  --JDS: Added BRAGFLO revision date. Write
C                                    BUNITS instead of UNITSC as units.
C                27-OCT-1993  --JSR: Adapted from JDS' version.
C                                    Added missing return statement.
C                25-OCT-1993  --JDS: Original version.
C
C   CALLED BY:   PROCBO
C
C   CALLS:       QAABORT
C
C   ARGUMENTS:
C   ENTRY/
C     --common blocks
C      /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C       NOUTFL  = Device no. of output diagnostics/debug file
C
C      /BRAG/   (from FORTRAN statement: INCLUDE 'brag.inc')
C       CPUNAM  = Computer name where BRAGFLO was run (CHARACTER*60)
C       REVDATE = Date of current BRAGFLO version (CHARACTER*8)
C
C      /UNTCNV/ (from FORTRAN statement: INCLUDE 'untcnv.inc')
C       BUNITS  = Units used in BRAGFLO output (CHARACTER*8)
C               = 'ENGLISH' or
C               = 'SI'
C
C     --through subroutine call
C       NAMELV2 = Array of element variable names (CHARACTER*8)
C       NAMGLV2 = Array of global variable names (CHARACTER*8)
C       NAMHIV2 = Array of history variable names (CHARACTER*8)
C       LABELEL = Array of element variable descriptions (CHARACTER*50)
C       LABELGL = Array of global variable descriptions (CHARACTER*50)
C       LABELHI = Array of history variable descriptions (CHARACTER*50)
C       LABUNTEL= Array of element variable units labels (CHARACTER*20)
C       LABUNTGL= Array of global variable units labels (CHARACTER*20)
C       LABUNTHI= Array of history variable units labels (CHARACTER*20)
C
C
C   LOCAL/
C       I       = DO loop control vbl.
C       BL      = CHARACTER*1 variable, a blank (' ').
C       N       = Number of 'extra' BRAGFLO information records.
C
C   EXIT/
C     --common block
C      /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C       NEXINFO = No. of BRAGFLO "extra" INFORMATION records (INTEGER)
C
C     --through subroutine call
C       EXINFO  = BRAGFLO "extra" information records array (CHAR*80)
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'brag.inc'
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'newana.inc'
      INCLUDE 'untcnv.inc'
      INTEGER I, N
      CHARACTER*(1)  BL
      CHARACTER*(8)  NAMELV2(*),  NAMGLV2(*),  NAMHIV2(*)
      CHARACTER*(20) LABUNTEL(*), LABUNTGL(*), LABUNTHI(*)
      CHARACTER*(50) LABELEL(*),  LABELGL(*),  LABELHI(*)
      CHARACTER*(80) EXINFO(*)
      DATA BL/' '/
C
C<><><><><><><><><><><>
C...Begin Procedures...
C<><><><><><><><><><><>
C
      N = 0
C     ...BRAGFLO "EXTRA" information; BRAGFLO revision date:
      N         = N + 1
      EXINFO(N) = 'Date of current BRAGFLO version:  '//REVDATE

C     ...BRAGFLO "EXTRA" information; BRAGFLO units record:
      N         = N + 1
      EXINFO(N) = 'BRAGFLO Units:  '//BUNITS

C     ...BRAGFLO "EXTRA" information; computer name record:
      N         = N + 1
      EXINFO(N) = 'Computer Name: '//CPUNAM

C     ...BRAGFLO "EXTRA" information; header records:
      IF (NVAREL2.GT.0)THEN
         N         = N + 1
         EXINFO(N) = BL
         N         = N + 1
         EXINFO(N) = 'Vbl Name Description of Element Variables  '//
     $               '                 Units Label         '
         N         = N + 1
         EXINFO(N) = '-------- ----------------------------------'//
     $               '---------------- --------------------'
      ENDIF

C     ...Names, descriptive labels, units labels for element variables
      DO 10 I=1,NVAREL2
         N              = N + 1
         EXINFO(N)(:80) = NAMELV2(I)(:8)//BL//LABELEL(I)(:50)//BL//
     $                    LABUNTEL(I)(:20)
   10 CONTINUE

C     ...BRAGFLO "EXTRA" information; header records:
      IF (NVAREL2.GT.0)THEN
         N         = N + 1
         EXINFO(N) = BL
         N         = N + 1
         EXINFO(N) = 'Vbl Name Description of History Variables  '//
     $               '                 Units Label         '
         N         = N + 1
         EXINFO(N) = '-------- ----------------------------------'//
     $               '---------------- --------------------'
      ENDIF

C     ...Names, descriptive labels, units labels for history variables
      DO 20 I=1,NVARHI2
         N              = N + 1
         EXINFO(N)(:80) = NAMHIV2(I)(:8)//BL//LABELHI(I)(:50)//BL//
     $                    LABUNTHI(I)(:20)
  20  CONTINUE

C     ...BRAGFLO "EXTRA" information; header records:
      IF (NVAREL2.GT.0)THEN
         N         = N + 1
         EXINFO(N) = BL
         N         = N + 1
         EXINFO(N) = 'Vbl Name Description of Global Variables   '//
     $               '                 Units Label         '
         N         = N + 1
         EXINFO(N) = '-------- ----------------------------------'//
     $               '---------------- --------------------'
      ENDIF

C     ...Names, descriptive labels, units labels for global variables
      DO 30 I=1,NVARGL2
         N              = N + 1
         EXINFO(N)(:80) = NAMGLV2(I)(:8)//BL//LABELGL(I)(:50)//BL//
     $                    LABUNTGL(I)(:20)
  30  CONTINUE

      IF(N.NE.NEXINFO)THEN
         WRITE(NOUTFL,*)
     $   ' *** ERROR counting BRAGFLO "extra" information records ***'
         WRITE(NOUTFL,*)
     $   '     Allocated (NEXINFO=',NEXINFO,') CHARACTER*80 records',
     $   ' in subroutine ADDMEM.'
         WRITE(NOUTFL,*)
     $   '     Counted N=',N,' CHARACTER*80 records in INFREC'
         WRITE(NOUTFL,*)
     $   '     Required that: N=NEXINFO !'
         CALL QAABORT('(INFREC) -- Error counting BRAGFLO extra info'//
     $   'rmation records ***')
      ENDIF
      RETURN
C
C-----------------------------------------------------------------------
C**** End of SUBOUTINE INFREC ****
C-----------------------------------------------------------------------
C
      END
