*COMDECK LOCAT
C***********************************************************************
C    /LOCATC/ (from FORTRAN statement: INCLUDE 'LOCAT.INC')
C     L[name]= Individual integer pointer to the base CHARACTER*1
C              array, C.   ( L[dynamic-array-name] )
C
C              NOTE: A FORTRAN equivalence statement is used for
C                    convenience to permit accessing the base
C                    CHARACTER*1 array, C, by BOTH a numeric index
C                    of dynamic arrays (LCPTR(index)) and by a single
C                    integer variable (L[name]).  It is convenient to
C                    key off an index, or range of indices when
C                    assigning memory to the base CHARACTER*1 array.
C                    However, in a subroutine argument list, clarity
C                    is maintained by using distinct integer variables
C                    as pointers of the base CHARACTER*1 array because
C                    they preceed the actual dynamic array name by the
C                    letter "L".
C
C***********************************************************************
C
C    /LOCATR/ (from FORTRAN statement: INCLUDE 'LOCAT.INC')
C     L[name]= Individual integer pointer to base REAL array, Q.
C              ( L[dynamic-array-name] )
C
C              NOTE: A FORTRAN equivalence statement is used for
C                    convenience to permit accessing the base
C                    REAL array, Q, by BOTH a numeric index of
C                    dynamic arrays (LRPTR(index)) and by a single
C                    integer variable (L[name]).
C
C***********************************************************************
C
C     Pointer   Description
C     -------   --------------------------------------------------------
C     LATTRIB = CAMDAT attributes values
C     LCOORD  = Coordinates array
C     LCNVFAC = BRAGFLO conversion factors array
C     LEXINFO = BRAGFLO "extra" information records array
C     LFILNAM = BRAGFLO file names
C     LIASATR = Attribute name truth table array
C     LIASPRP = Property name truth table array
C     LICONN  = Connectivity for the block
C     LIDEBLK = Integer ID for element block
C     LIEBLOC = Special element and element block number array
C     LINFO   = Optional information records
C     LISEVOK2= Array map of element variables output to CAMDAT
C     LMAP    = Optimized element array
C     LNAMATR = Attribute names
C     LNAMECO = Coordinate names
C     LNAMELB = Element block names
C     LNAMELV2= Names for POSTBRAG CAMDAT element variables
C     LNAMHIV2= Names of POSTBRAG CAMDAT history variables
C     LNAMGLV2= Names of POSTBRAG CAMDAT global variables
C     LNMANDV = Names of nodal variables
C     LNATRIB = Number of attributes for element block
C     LNELNOD = Number of nodes defining element connectivity in block
C     LNMATPR = Material property names
C     LNUMATR = Number of attributes per material
C     LNUMELB = Number of elements per material
C     LNUMPRP = Number of material properties per material
C     LQAINFO = QA information
C     LVALELV2= POSTBRAG CAMDAT element variables
C     LVALHIV2= POSTBRAG CAMDAT history variables
C     LVALGLV2= POSTBRAG CAMDAT global variables
C     LUNTCNVEL= Conversion factors from BRAGFLO to SI units, elem vbls
C     LUNTCNVHI= Conversion factors from BRAGFLO to SI units, hist vbls
C     LUNTCNVGL= Conversion factors from BRAGFLO to SI units, glob vbls
C
C***********************************************************************
C234567
C     ...Real Dynamic Base-array (Q) pointers
      INTEGER
     $LNUMELB,
     $LNATRIB,   LIASATR,   LATTRIB,
     $LIEBLOC
      COMMON /LOCATR/
     $LNUMELB,
     $LNATRIB,   LIASATR,   LATTRIB,
     $LIEBLOC

C     ...Character Dynamic Base-array (C) pointers
      INTEGER
     $LQAINFO,   LINFO,     LNAMATR,
     $LFILNAM
      COMMON /LOCATC/
     $LQAINFO,   LINFO,     LNAMATR,
     $LFILNAM
C***********************************************************************

