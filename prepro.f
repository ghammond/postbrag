*DECK PREPRO
      SUBROUTINE PREPRO( FIDB, FBRA, FODB )
C***********************************************************************
C***                                                                ****
C***     P_R_E  -  P_R_O_cess  all  input/output  files  module     ****
C***     - - -     - - -                                            ****
C***********************************************************************
C
C  PURPOSE:    Input pre-processor that:
C              1.  Performs QA functions.
C              2.  Uses standard routines from CAMCON_LIB for file
C                  management.
C
C  AUTHOR:     Jonathan S. Rath
C
C  UPDATED:    01-FEB-1996  --JEB: Add call to QAFETCH
C              11-AUG-1995  --JEB: Removed unreferenced variables
C              31-JUL-1995  --JEB: Modified subroutine for use in
C                                  Configuration Management System (CMS)
C                                  File names are logicals defined in command
C                                  files or through command line
C                                  or prompted input
C              20-SEP-1993  --JDS: Adapted to BRAGFLO version >2.30VV.
C                                  Option to specify fixed-length-record
C                                  BRAGFLO binary file no longer used;
C                                  assumes variable-length unless flag
C                                  in binary file changes it to fixed.
C              05-AUG-1993  --JSR: First Ed. for POSTBRAG_T
C
C  CALLED BY:  POSTBRAG {main program}
C
C  CALLS:      QASETUP
C              QAFETCH
C              QABANNER
C              FILDFNAM
C              FILRDNAMS
C              FILWRNAMS
C              FILOPEN
C              IQAERRUNI
C              QAABORT
C              QAPAGE
C              QADOEDIS
C
C  ARGUMENTS:
C  ENTRY/
C   --common blocks
C
C  LOCAL/
C     IERR   = Integer error flag
C     IOUT   = Integer output file cancel flag
C
C  EXIT/
C   --common block
C    /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C     NOUTFL = Device no. of diagnostics/debug output file
C     NBINF  = Determines BRAGFLO output file type:
C            = 0: ASCII output file
C            = 1: BINARY output file, variable length records
C            = 2: BINARY output file, fixed record length
C
C   --through subroutine call
C     FIDB   = input CAMDAT name
C     FBRA   = BRAGFLO output filename
C     FODB   = output CAMDAT name
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'fileun.inc'
      INTEGER  IERR
      CHARACTER*(*)   FBRA, FIDB, FODB
      CHARACTER*(132) FILESP(5)
C
C<><><><><><><><><><><>
C...BEGIN PROCEDURES...
C<><><><><><><><><><><>
C
      NOUTFL = 7

      CALL QASETUP('POSTBRAG', ' ', ' ',
     1             'James D. Schreiber & Jonathan S. Rath', ' ')

C     ... Write welcome QABANNER to terminal
      CALL QABANNER(0,'A post-processor program for BRAGFLO',
     +   ' ',' ')

C     ...Define files for FILRDNAMS
      CALL FILDFNAM('Input CAMDAT File','INP','REQ',
     $              '-input', ' ')
      CALL FILDFNAM('Letter A if BRAGFLO output file is ASCII, '//
     $              'B if binary', 'TEXT', 'REQUIRED', '-ascii', 'A')
      CALL FILDFNAM('Input BRAGFLO Output File','INP','REQ',
     $              '-xfer', ' ')
      CALL FILDFNAM('Output CAMDAT File','OUT','REQ',
     $              '-output', ' ')
      CALL FILDFNAM('Output Diagnostics File','OUT','OPT',
     +              '-debug', 'POSTBRAG.dbg')
C
C     ...Read in the names of the files
      CALL FILRDNAMS(FILESP,IERR)
      IF(IERR.NE.0)THEN
         CALL FILWRNAMS(0,FILESP)
         CALL QAABORT('**** Incorrect file assignments ****')
      ENDIF
C
      IF(filesp(5)(:1).EQ.' ')THEN
C     ...open scratch file for POSTBRAG debug file
        CALL filopen(NOUTFL,'SCR', 'FORM','POSTBRAG.DBG', IERR)
      ELSE
C     ...open user specified POSTBRAG debug file
        CALL filopen(NOUTFL,'OUT', 'FORM',FILESP(5), IERR)
      ENDIF

C
C     ...Set the DEVICE number of the Diagnostics/debug output file
      CALL IQAERRUNI(NOUTFL)
C
C     ...Write headers, QABANNER, DOE disclaimer, and file assignments
      IF(NOUTFL.GT.0)THEN
         CALL QAPAGE(NOUTFL,' ')
         CALL QABANNER(NOUTFL,'A post-processor program for '//
     +                 'BRAGFLO',' ',' ')
         CALL QADOEDIS(NOUTFL,'*')
         CALL FILWRNAMS(NOUTFL,FILESP)
      ENDIF

C     ...Determine which type of BRAGFLO output file will be used
      CALL STRUPCASE (FILESP(2))
      IF(FILESP(2)(:1).EQ.'A') THEN
C        ...Using the BRAGFLO ASCII file
         NBINF = 0
      ELSE
C        ...Using the binary file with variable record lengths
         NBINF = 1
C        ...Flag in the binary file may change this to NBINF=2,
C        ...i.e., to use the binary file with fixed record lengths
      ENDIF

      FIDB = FILESP(1)(1:132)
      FBRA = FILESP(3)(1:132)
      FODB = FILESP(4)(1:132)

      RETURN
C-----------------------------------------------------------------------
C**** End of SUBROUTINE PREPRO ****
C-----------------------------------------------------------------------
      END
