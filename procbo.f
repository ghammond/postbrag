*DECK PROCBO
!apg Pass dynamic memory arrays
      SUBROUTINE PROCBO( FILNAM, INFO, 
     $                   NUMELB, NATRIB, NAMATR, IASATR, ATTRIB, IEBLOC,
     $                   TIME, HIFLAG )
C***********************************************************************
C***                                                                ****
C***           P_R_O_C_ess  B_RAGFLO  O_utput  module               ****
C***           - - - -      -         -                             ****
C***********************************************************************
C
C  PURPOSE:    Reads the BRAGFLO output data, and writes analysis
C              data to the output CAMDAT.
C
C  AUTHOR:     James D. Schreiber
C
C  UPDATED:    08-NOV-1993  --JDS: Deleted all but one argument in call
C                                  to SETNAM.
C              25-OCT-1993  --JDS: Adapted to POSTBRAG for BRAGFLO
C                                  version >2.30VV.
C              05-AUG-1993  --JSR: Adapted to new CAMDAT_LIB and
C                                  CAMCON_LIB routines and to BRAGFLO_T.
C                                  Allocated memory dynamically to all
C                                  arrays.  Made all 1-dimensional. More
C                                  error handling and diagnostics.
C                                  Made all records <= 72 characters
C              02-SEP-1992  --JDS: Deleted NUMELB array from ATTDXY
C                                  arg list; deleted NAMGLV & ISEVOK
C                                  arrays from RBRAGO argument list,
C                                  since they weren't used.
C              28-AUG-1992  --JDS: Added NEWVAR.INC; added GVNAM to
C                                  SETNAM argument list to transfer
C                                  global variable names from
C                                  REDMAP so they can be added to
C                                  CAMDAT array NAMGLV.
C              04-MAR-1992  --JDS: Deleted unused local variables:
C                                  I, KKPR.
C              26-FEB-1992  --JDS: Fixed check for "Elapsed Time"
C                                  to Upper & Lower case.
C              12-NOV-1991  --JDS: Added Q(LRVARS) to RBRAGO call list.
C              18-JUL-1991  --JDS: Finished adapting to BRAGFLO.
C              12-JUL-1991  --JDS: Moved argument list into UNTCNV.INC.
C              11-JUL-1991  --JDS: Set dimension of MAPOUT to *;
C                                  deleted use of NWAT in calls to
C                                  SETNAM & RBRAGO; added
C                                  NMAP & array NAMES to argument
C                                  lists of PROCBO & SETNAM.
C              09-MAY-1991  --JDS: Converted to BRAGFLO
C              21-FEB-1991  --JDS: Added MAPOUT array
C              07-FEB-1991  --JDS: Cleaned up use of binary file
C              23-FEB-1990  --JDS: Added binary output file
C                                  reading option
C              01-FEB-1990  --JDS: Original version
C
C  CALLED BY:  POSTBRAG {main program}
C
C  CALLS:      REDMAP
C              REDMAP1
C              SETNAM
C              INFREC
C              ATTDXY
C              WRCHEA
C              RBRAGO
C              WRCANA
C              QAABORT
C
C  ARGUMENTS:
C   ENTRY/
C    --common blocks
C     /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C      IBOSTO = Device no. of BRAGFLO transfer file
C      NBINF  = Determines BRAGFLO output file type:
C             = 0;ASCII output file
C             = 1;BINARY output file, written on VAX
C             = 2;BINARY output file, written on Alliant
C      NOUTFL = Device no. of output diagnostics/debug file
C
C     /NEWANA/ (from FORTRAN statement: INCLUDE 'newana.inc')
C      NVAREL2= Number of NEW CAMDAT ELement variables
C      NVARGL2= Number of NEW CAMDAT GLobal  variables
C      NVARHI2= Number of NEW CAMDAT HIstory variables
C
C     /UNTCNV/ (from FORTRAN statement: INCLUDE 'untcnv.inc')
C      BUNITS = BRAGFLO type units (CHARACTER*8)
C             = 'ENGLISH' or
C             = 'SI'
C
C    --through subroutine call
C      FILNAM = 
C      INFO   = Additional/Optional information array (CHAR*80)
C      NUMELB = No. of elements in each element block array
C      NATRIB = No. of attributes in each material array (INTEGER)
C      NAMATR = Names of the attributes (CHAR*8)
C      IASATR = Logical array for attributes in materials
C      ATTRIB = Attribute values array (REAL)
C      IEBLOC = Global element/element block special CONNECTIVITY array
C
C   LOCAL/
C      IERR   = INTEGER error code
C      CLINE  = CHARACTER string;
C               One full line of BRAGFLO output.
C      HIFLAG = History variable flag:
C             = FALSE; all variables output
C             > TRUE; only HISTORY variables output
C      LEND   = End-of-file indicator in BRAGFLO output file:
C             = 0; No E-O-F found yet; write results to CAMDAT file.
C             = 1; E-O-F found; normal termination of BRAGFLO output.
C             = 2; E-O-F found in middle of analysis output;
C                  do NOT write last (partial) results to CAMDAT.
C      NSTPH  = Number of time steps (history vbl outputs) read.
C               (Should be same as NSTEP).
C      NSUMM  = Number of BRAGFLO element and global vbl outputs read.
C      TIME   = Elapsed time found in BRAGFLO output.
C
C   EXIT/
C      none
C
C***********************************************************************
C234567
      IMPLICIT  NONE
      INCLUDE 'brag.inc'
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'newana.inc'
      INCLUDE 'untcnv.inc'

      CHARACTER*80 FILNAM(*)
      CHARACTER*80 INFO(*)
      INTEGER NUMELB(*)
      INTEGER NATRIB(*)
      CHARACTER*8 NAMATR(*)
      LOGICAL IASATR(*)
      DOUBLE PRECISION ATTRIB(*)
      INTEGER IEBLOC(*)
      DOUBLE PRECISION TIME  !apg REAL
      LOGICAL HIFLAG  !apg was REAL

      INTEGER LEND, NSTPH, NSUMM
      CHARACTER*(132) CLINE
      DOUBLE PRECISION, ALLOCATABLE :: RVARS(:)  !apg REAL
      DOUBLE PRECISION, ALLOCATABLE :: UNTCNVEL(:)  !apg REAL
      DOUBLE PRECISION, ALLOCATABLE :: UNTCNVHI(:)  !apg REAL
      DOUBLE PRECISION, ALLOCATABLE :: UNTCNVGL(:)  !apg REAL
      DOUBLE PRECISION, ALLOCATABLE :: VALELV2(:)  !apg REAL
      DOUBLE PRECISION, ALLOCATABLE :: VALHIV2(:)  !apg REAL
      DOUBLE PRECISION, ALLOCATABLE :: VALGLV2(:)  !apg REAL
      LOGICAL, ALLOCATABLE :: ISEVOK2(:)
      DOUBLE PRECISION, ALLOCATABLE :: CNVFAC(:)  !apg REAL
      INTEGER, ALLOCATABLE :: NGBHIV(:)
      INTEGER, ALLOCATABLE :: IJKHIV(:)
      CHARACTER*8, ALLOCATABLE :: NAMELV2(:)
      CHARACTER*8, ALLOCATABLE :: NAMHIV2(:)
      CHARACTER*8, ALLOCATABLE :: NAMGLV2(:)
      CHARACTER*50, ALLOCATABLE :: LABELEL(:)
      CHARACTER*50, ALLOCATABLE :: LABELHI(:)
      CHARACTER*50, ALLOCATABLE :: LABELGL(:)
      CHARACTER*20, ALLOCATABLE :: LABUNTEL(:)
      CHARACTER*20, ALLOCATABLE :: LABUNTHI(:)
      CHARACTER*20, ALLOCATABLE :: LABUNTGL(:)
      CHARACTER*80, ALLOCATABLE :: EXINFO(:)
      CHARACTER*80, ALLOCATABLE :: INFO2(:)
C
C<><><><><><><><><><><>
C...Begin Procedures...
C<><><><><><><><><><><>
C
      CLINE = ' '
      NSTPH = 0
      NSUMM = 0

      ALLOCATE (RVARS(NUMEL))
      RVARS = 1.0  !apg
      ALLOCATE (UNTCNVEL(NVAREL2))
      UNTCNVEL = 1.0  !apg
      ALLOCATE (UNTCNVHI(NVARHI2))
      UNTCNVHI = 1.0  !apg
      ALLOCATE (UNTCNVGL(NVARGL2))
      UNTCNVGL = 1.0  !apg
      ALLOCATE (VALELV2(NVAREL2*NUMEL))
      VALELV2 = 1.0  !apg
      ALLOCATE (VALHIV2(NVARHI2))
      VALHIV2 = 1.0  !apg
      ALLOCATE (VALGLV2(NVARGL2))
      VALGLV2 = 1.0  !apg
      ALLOCATE (ISEVOK2(NVAREL2*NELBLK))
!apg Change length of CNVFAC from NUMEL to NUQATR2*NELBLK
      ALLOCATE (CNVFAC(NUQATR2*NELBLK))
      CNVFAC = 1.0  !apg
      ALLOCATE (NGBHIV(NHIV))
      ALLOCATE (IJKHIV(3*NHIEL))
      ALLOCATE (NAMELV2(NVAREL2))
      NAMELV2 = ' '  !apg
      ALLOCATE (NAMHIV2(NVARHI2))
      NAMHIV2 = ' '  !apg
      ALLOCATE (NAMGLV2(NVARGL2))
      NAMGLV2 = ' '  !apg
      ALLOCATE (LABELEL(NVAREL2))
      LABELEL = ' '  !apg
      ALLOCATE (LABELHI(NVARHI2))
      LABELHI = ' '  !apg
      ALLOCATE (LABELGL(NVARGL2))
      LABELGL = ' '  !apg
      ALLOCATE (LABUNTEL(NVAREL2))
      LABUNTEL = ' '  !apg
      ALLOCATE (LABUNTHI(NVARHI2))
      LABUNTHI = ' '  !apg
      ALLOCATE (LABUNTGL(NVARGL2))
      LABUNTGL = ' '  !apg
      NEXINFO    = 12+NVARHI2+NVARGL2+NVAREL2
      NXINFO2    = NXINFO+NUMFIL+NEXINFO
      ALLOCATE (EXINFO(NEXINFO))
      EXINFO = ' '  !apg
      ALLOCATE (INFO2(NXINFO2))
      INFO2 = ' '  !apg

C     ...Get size of GLOBAL variables array
      CALL REDMAP( NGBHIV,   IJKHIV,    NAMELV2,
     $              NAMGLV2,   NAMHIV2,   UNTCNVEL,
     $              UNTCNVGL,  UNTCNVHI,  LABELEL,
     $              LABELGL,   LABELHI,   LABUNTEL,
     $              LABUNTGL,  LABUNTHI )

C     ...Set names and corresponding ISEVOK values
      CALL SETNAM( ISEVOK2 )

C     ...Get new attributes
      CALL ATTDXY( NUMELB, NATRIB, IASATR,
     $             IEBLOC,  RVARS,   NAMATR,
     $             ATTRIB,  CNVFAC )

C     ...Assemble new information records
      CALL INFREC( EXINFO,  NAMELV2,  NAMGLV2,
     $             NAMHIV2, LABELEL,  LABELGL,
     $             LABELHI, LABUNTEL, LABUNTGL,
     $             LABUNTHI )

C     ...Write the new CAMDAT file header and sizing records
C        (NO analysis)
      CALL WRCHEA(  INFO,
     $              FILNAM,  EXINFO,  INFO2,
     $             IASATR,  NAMATR, ATTRIB,
     $             NAMHIV2, NAMGLV2,
     $              NAMELV2 )
C
C**** Read BRAGFLO output and write ANALYSIS info to CAMDAT ****
C
  10  CONTINUE
      IF(NBINF.EQ.0)THEN
         READ(IBOSTO,'(A)',END=200) CLINE
      ENDIF

      IF((NBINF.EQ.0.AND.CLINE(5:16).EQ.'Elapsed Time')
     $    .OR.(NBINF.GT.0)) THEN

C        ...This begins output of initial conditions
C        ...Backspace so RBRAGO can find keywords 'ELAPSED TIME'
         IF(NBINF.EQ.0)BACKSPACE (IBOSTO)
         WRITE(NOUTFL,1000)
         CALL RBRAGO(  LABELEL,  NAMELV2,  VALELV2,
     $                 VALGLV2,  VALHIV2,  UNTCNVHI,
     $                 UNTCNVGL, UNTCNVEL, TIME,
     $                 HIFLAG,       LEND,         IEBLOC,
     $                 NSUMM,        RVARS )

C        ...Check for E-O-F during reading of initial conditions
         IF(LEND.GT.0)GOTO 200
         WRITE(NOUTFL,1010)
         GOTO 20

      ENDIF
C     ...Read next record of BRAGFLO output
      GOTO 10

C     ...Write initial conditions from BRAGFLO output results
C        results to CAMDAT
  20  CONTINUE
      NSTPH = NSTPH + 1
      WRITE(NOUTFL,1040)NSTPH,NSUMM,TIME
      CALL WRCANA(  TIME,
     $              HIFLAG,      NAMHIV2, NAMGLV2,
     $              NAMELV2, VALHIV2, VALGLV2,
     $              VALELV2 )

C     ...Read one step of the BRAGFLO output results
C     ...(Elapsed Time, Pressures, Saturations, etc.)
 100  CONTINUE
      CALL RBRAGO(  LABELEL,  NAMELV2,  VALELV2,
     $              VALGLV2,  VALHIV2,  UNTCNVHI,
     $              UNTCNVGL, UNTCNVEL, TIME,
     $              HIFLAG,       LEND,         IEBLOC,
     $              NSUMM,        RVARS )

      IF(LEND.EQ.2)THEN
C        ...EOF found before this output step completely read;
C        ...EXIT now and DO NOT write partial results to CAMDAT
         WRITE(NOUTFL,1020)
C        ...Exit routine
         GOTO 999
      ELSEIF(LEND.EQ.1)THEN
         WRITE(NOUTFL,1030)
C        ...Exit routine
         GOTO 999
      ENDIF

C     ...Write analysis results to CAMDAT file for current step
      NSTPH = NSTPH + 1
      WRITE(NOUTFL,1040)NSTPH,NSUMM,TIME
      CALL WRCANA(  TIME,
     $              HIFLAG,      NAMHIV2, NAMGLV2,
     $              NAMELV2, VALHIV2, VALGLV2,
     $              VALELV2 )

C     ...Continue reading BRAGFLO output
      GOTO 100

C     ... END-OF-FILE found
 200  CONTINUE
      WRITE(NOUTFL,1050)
      CALL QAABORT('(PROCBO) - EOF occurred before end of '//
     $             'initialization ***')

 999  CONTINUE
      deALLOCATE (UNTCNVEL)
      deALLOCATE (UNTCNVHI)
      deALLOCATE (UNTCNVGL)
      deALLOCATE (VALELV2)
      deALLOCATE (VALHIV2)
      deALLOCATE (VALGLV2)
      deALLOCATE (ISEVOK2)
      deALLOCATE (CNVFAC)
      deALLOCATE (NGBHIV)
      deALLOCATE (IJKHIV)
      deALLOCATE (NAMELV2)
      deALLOCATE (NAMHIV2)
      deALLOCATE (NAMGLV2)
      deALLOCATE (LABELEL)
      deALLOCATE (LABELHI)
      deALLOCATE (LABELGL)
      deALLOCATE (LABUNTEL)
      deALLOCATE (LABUNTHI)
      deALLOCATE (LABUNTGL)
      deALLOCATE (EXINFO)
      deALLOCATE (INFO2)

      RETURN
C
C---------------  F O R M A T   S T A T E M E N T S  ------------------
C
1000  FORMAT(' *** [PROCBO] Go to RBRAGO and read Initial Conditions',
     $       ' ***')
1010  FORMAT(' *** [PROCBO] Initial Conditions completely read ***',/,
     $       ' *** [PROCBO] proceeding with 1st time step ***')
1020  FORMAT(' *** [PROCBO] EOF found before this step',
     $       ' completely read ***',/,
     $       '              Quit now; don''t write  partial results',
     $       ' to CAMDAT.')
1030  FORMAT(' *** [PROCBO] Normal End of BRAGFLO output found ***')
1040  FORMAT(' *** [PROCBO] Read HVAR step no.',I5,'; EVAR output no.',
     $       I4,'; Time =',1PE13.6)
1050  FORMAT(' *** [PROCBO] EOF Read in RBRAGO or in subroutine ',
     $       'called from RBRAGO ***',/,
     $       ' *** Occurred before End of Initialization ***')
C
C----------------------------------------------------------------------
C**** End of SUBROUTINE PROCBO *****
C----------------------------------------------------------------------
C
      END
