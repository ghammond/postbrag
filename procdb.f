*DECK PROCDB
!apg Pass dynamic memory arrays
      SUBROUTINE PROCDB( QAINFO, INFO, 
     $                   NUMELB, NATRIB, NAMATR, IASATR, ATTRIB, IEBLOC,
     $                   TIME0,  HIFLAG0 )
C***********************************************************************
C****                                                               ****
C****           P_R_O_C_ess  input D_ata  B_ase  routine            ****
C****           - - - -            -      -                         ****
C***********************************************************************
C
C  PURPOSE:    Process dynamic memory allocation for input
C              CAMDAT file etc...
C
C  AUTHOR:     Jonathan S. Rath
C
C  UPDATED:    26-OCT-1993  --JDS: Adapted to BRAGFLO version >2.30VV;
C                                  cosmetic changes only.
C              05-AUG-1993  --JSR: First Ed. for POSTBRAG_T.
C
C  CALLED BY:  POSTBRAG {main program}
C
C  CALLS:      DBISIZES
C              DBINVAR
C              REAHEA
C              GIEBLO
C              RCTIM0
C              QAABORT
C
C  ARGUMENTS:
C  ENTRY/
C   --common blocks
C    /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C     IDB    = Device no of Input CAMDAT
C     ODB    = "            Output CAMDAT
C
C   --through subroutine call
C
C  EXIT/
C   --common blocks
C    /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C     HEADER = Character description of problem, etc...
C     LESSEL = List of Element Side sets concatenated Element List
C     LESSNL = List of Element Side sets concatenated Nodes associated
C              with each element side set Node List
C     LNPSNL = List of Nodal Point set concatenated Nodes List
C     MAXNENO= Max. no. of nodes of any element
C     NDIM   = Number of physical DIMensions
C     NELBLK = Number of ELement BLocKs in mesh
C     NX     = Number of elements along X-axis of mesh
C     NY     = Number of elements along Y-axis of mesh
C     NZ     = Number of elements along Z-axis of mesh
C     NXY    = NX*NY
C     NQAREC = Number of QA records
C     NUMEL  = NUmber of ELeMents in mesh
C     NUMESS = NUMber of Element Side set in mesh
C     NUMNOD = NUMber of NODes in mesh
C     NUMNPS = NUMber of Nodal Point set in mesh
C     NUQATR = Number of UniQue AtTRibute names
C     NUQATR2= Number of UniQue AtTRibute names + 1
C     NUQPRP = Number of UniQue PRoPerty names
C     NVAREL = Number of ELEMENT type ANALYSIS variables
C     NVARGL = Number of GLOBAL  "                     "
C     NVARHI = Number of HISTORY "                     "
C     NVARND = Number of NODAL   "                     "
C     NXINFO = Number of INFO records
C
C   --through subroutine call
C     QAINFO = Quality Assurance array (CHAR*8)
C     INFO   = Additional/Optional information array (CHAR*80)
C     NUMELB = No. of elements in each element block array
C     NATRIB = No. of attributes in each material array (INTEGER)
C     NAMATR = Names of the attributes (CHAR*8)
C     IASATR = Logical array for attributes in materials
C     ATTRIB = Attribute values array (REAL)
C     IEBLOC = Global element/element block special CONNECTIVITY array
C     TIME0  = First time-step value used in CAMDAT (REAL)
C     HIFLAG0= First HISTORY FLAG used in CAMDAT (LOGICAL)
C     BUNITS = Units system used in BRAGFLO output (SI or English).
C     MAPOUT = Array of output control codes indicating whether each
C              Element variable in BRAGFLO was printed to output.
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'brag.inc'
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'untcnv.inc'

      CHARACTER*8 QAINFO(*)
      CHARACTER*80 INFO(*)
      INTEGER NUMELB(*)
      INTEGER NATRIB(*)
      CHARACTER*8 NAMATR(*)
      LOGICAL IASATR(*)
      DOUBLE PRECISION ATTRIB(*)
      INTEGER IEBLOC(*)
      LOGICAL HIFLAG0  !apg was REAL
      DOUBLE PRECISION TIME0  !apg REAL

      DOUBLE PRECISION, ALLOCATABLE :: COORD(:)  !apg REAL
      INTEGER, ALLOCATABLE :: MAP(:)
      INTEGER, ALLOCATABLE :: NELNOD(:)
      INTEGER, ALLOCATABLE :: NUMPRP(:)
      INTEGER, ALLOCATABLE :: ICONN(:)
      LOGICAL, ALLOCATABLE :: IASPRP(:)
      DOUBLE PRECISION, ALLOCATABLE :: XMATPR(:)  !apg REAL
      CHARACTER*8, ALLOCATABLE :: NAMECO(:)
      CHARACTER*8, ALLOCATABLE :: NAMELB(:)
      CHARACTER*8, ALLOCATABLE :: NMATPR(:)
C
C<><><><><><><><><><><>
C...Begin Procedures...
C<><><><><><><><><><><>
C
!apgC     ...Read input CAMDAT file and get array sizes  !apg Move to POSTBRAG
!apg      CALL DBISIZES( IDB,     NQAREC,  NXINFO,  NDIM,   NUMNOD,
!apg     1               NELBLK,  NUMEL,   NX,      NY,     NZ,
!apg     2               MAXNENO, NUQATR,  NUQPRP,  NUMNPS, LNPSNL,
!apg     3               NUMESS,  LESSEL,  LESSNL,  IERR )
!apg      IF(IERR.NE.0)CALL QAABORT('(PROCDB) - reading "HEADER" size '//
!apg     $                          ' parameters and variables ***')
!apg      CALL DBINVAR( IDB, NVARHI, NVARGL, NVARND, NVAREL, IERR )
!apg      IF(IERR.NE.0)CALL QAABORT(
!apg     $'(PROCDB) - reading number of ANALYSIS variables ***')

!apgC     ...Add an extra attribute for GRIDVOLume
!apg      NUQATR2 = NUQATR+1

!apgC     ...Reserve dynamic arrays  !apg Move to POSTBRAG

C     ...Reserve local dynamic arrays
      ALLOCATE (COORD(NUMNOD*NDIM))
      COORD = 1.0  !apg
      ALLOCATE (MAP(NUMEL))
      ALLOCATE (NELNOD(NELBLK))
      ALLOCATE (NUMPRP(NELBLK))
      ALLOCATE (ICONN(MAXNENO*NUMEL))
      ALLOCATE (IASPRP(NUQPRP*NELBLK))
      ALLOCATE (XMATPR(NUQPRP*NELBLK))
      XMATPR = 1.0  !apg
      ALLOCATE (NAMECO(NDIM))
      NAMECO = ' '  !apg
      ALLOCATE (NAMELB(NELBLK))
      NAMELB = ' '  !apg
      ALLOCATE (NMATPR(NUQPRP))
      NMATPR = ' '  !apg

C     ...Read various CAMDAT "header" sections into arrays
      CALL REAHEA( COORD,  MAP,
     $            NUMELB, NELNOD, NUMPRP, NATRIB,
     $            IASPRP, IASATR, ICONN,   XMATPR,
     $            ATTRIB,  NAMELB,  NMATPR,  NAMATR,
     $             NAMECO,  INFO,    QAINFO )

C     ...Compute other functions involving NX, NY, NZ
      NXY  = NX*NY

C     ...optional info. records containing supportive documentation
C     ...INFO(1) contains the RANDOM seed used for Monte Carlo sampling
C     ...INFO(2) contains integers WHICH SPECIFY THE NUMBER OF ELEMENTS
C     ...INFO(3) contains the type of UNITS used throughout computations
C     ...If NXINFO>2, store INFO(3) in UNITSC;
C        Otherwise, use default (SI)
      UNITSC(:8) = 'SI      '
      IF(NXINFO.GE.3)THEN
!apg         I = LINFO+2*80-1
!apg         DO 10 J=1,8
!apg            UNITSC(J:J) = C(I+J)
!apg  10     CONTINUE
         UNITSC(1:8) = INFO(3)(1:8)  !apg
         IF(UNITSC(:2).NE.'SI'.AND.UNITSC(:3).NE.'ENG'.AND.
     $      UNITSC(:2).NE.'si'.AND.UNITSC(:3).NE.'eng'     )THEN
            UNITSC(:8) = 'SI      '
!apg            DO 20 J=1,8
!apg               C(I+J) = UNITSC(J:J)
!apg  20        CONTINUE
            INFO(3)(1:8) = UNITSC(1:8)  !apg
         ENDIF
      ENDIF

C     ...Generate Inverse Element tranformation + element block array
      CALL GIEBLO( MAP, NUMELB, IEBLOC )

C     ...Read various CAMDAT "analysis/times" section into arrays
      CALL RCTIM0( TIME0, HIFLAG0 )

      deALLOCATE (COORD)
      deALLOCATE (MAP)
      deALLOCATE (NELNOD)
      deALLOCATE (NUMPRP)
      deALLOCATE (ICONN)
      deALLOCATE (IASPRP)
      deALLOCATE (XMATPR)
      deALLOCATE (NAMECO)
      deALLOCATE (NAMELB)
      deALLOCATE (NMATPR)

      RETURN
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE PROCDB *****
C-----------------------------------------------------------------------
C
      END
