*DECK QABRAG
      SUBROUTINE QABRAG( QAINFO )
C***********************************************************************
C***                                                                ****
C***   Gets Q_uality  A_ssurance records from B_R_A_G_FLO output    ****
C***        -         -                       - - - -               ****
C***********************************************************************
C
C   PURPOSE:      Assembles QA information records from BRAGFLO output
C                 file.  QABRAG is called before any of the output file
C                 has been read, because all QA is at the top .
C
C   AUTHOR:       James D. Schreiber
C
C   UPDATED:      09-JAN-1996  --JEB: Added ability to read PREBRAG QA records
C                                     written to either the binary or ASCII 
C                                     output results files.
C                 10-JUN-1994  --JDS: Corrected internal read to convert
C                                     STRING(34:37) to NUMFIL when read-
C                                     ing BRAGFLO ASCII output file.
C                 16-SEP-1993  --JDS: Adapted to POSTBRAG for BRAGFLO
C                                     version >2.30VV.
C                 05-AUG-1993  --JSR: Adapted from original POSTBRAG
C                                     for POSTBRAG_T.
C                 
C
C   LIMITATIONS:  Formats of date and time found in output file
C                 follows VAX conventions.
C
C   CALLED BY:    POSTBRAG (main program)
C
C   CALLS:        QAABORT
C                 QAMAXERR
C
C   ARGUMENTS:
C   ENTRY/
C     --common blocks
C      /CALEND/ (from FORTRAN statement: INCLUDE 'calend.inc')
C       MONTH  = Month abbreviation, VAX CHARACTER*3 FORMAT.
C
C      /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C       NQAREC = Number of QA records
C
C     --through subroutine call
C       QAINFO = QA information array (character data).
C
C   LOCAL/
C       MN     = Month number (CHARACTER variable).
C       MNDAYR = Date, with format: MN/DA/YR.
C       NNN    = Scratch INTEGER array for reading line of binary file.
C       PRGRAM = Program name.
C       TIME   = Time of day when run initiated, with format hr:mi:se.
C       STRING = Scratch CHARACTER variable.
C       VRSION = Program version number.
C
C   EXIT/
C     --common blocks
C      /BRAG/ (from FORTRAN statement: INCLUDE 'brag.inc')
C       MAXFIL = Max. no. of files to ever be used by BRAGFLO (INTEGER)
C       NUMFIL = Number of files used by BRAGFLO (INTEGER)
C
C      /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C       NQAREC = Number of QA records
C       LQAPB  = Logical variable that if true indicated that PREBRAG
C                QA records were found on the BRAGFLO binary or ASCII
C                output results file
C     --through subroutine call
C       QAINFO = updated QA information array (character data).
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'brag.inc'
      INCLUDE 'calend.inc'
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'untcnv.inc'
      INCLUDE 'newana.inc'
      INTEGER  I, IREC
      CHARACTER*(1)  BFILTYP
      CHARACTER*(2)  MN
      CHARACTER*(8)  MNDAYR, PRGRAM, TIME, VRSION
      CHARACTER*(8)  PB_MNDAYR, PB_PRGRAM, PB_TIME, PB_VRSION, PB_DATE
      CHARACTER*(8)  QAINFO(4,*)
      CHARACTER*(80) STRING
C
C<><><><><><><><><><><>
C...BEGIN PROCEDURES...
C<><><><><><><><><><><>
C
      REWIND(IBOSTO)

C     ...Initialize logical LQAPB to false
      LQAPB = .FALSE.

      IF( NBINF.GE.1 )THEN

C     ...Binary BRAGFLO output file,
C     ...READ PREBRAG QA Records if they are present on the Binary file
         READ(IBOSTO,END=80) STRING(1:8)
         IF(STRING(1:8).EQ.'PREBRAG ')THEN
            LQAPB = .TRUE.
C        ...READ PREBRAG QA Records
C        ...Program name    
            PB_PRGRAM = STRING(1:8)
C        ...Program version number
            READ(IBOSTO,END=80) PB_VRSION
C        ...Program revision date
            READ(IBOSTO,END=80) PB_DATE
C        ...Date PREBRAG was run
            READ(IBOSTO,END=80) PB_MNDAYR
C        ...Time PREBRAG was run
            READ(IBOSTO,END=80) PB_TIME
C        ...Read the first BRAGFLO QA record
            READ(IBOSTO,END=80) MNDAYR
         ELSE
            MNDAYR = STRING(1:8)
         ENDIF

C        ...Read the remaining records to get the BRAGFLO QA information

C        ...Time run initiated
         READ(IBOSTO,END=80) TIME

C        ...Program name
         READ(IBOSTO,END=80) PRGRAM

C        ...Program version number
         READ(IBOSTO,END=80) VRSION

C        ...Program revision date
         READ(IBOSTO,END=80) REVDATE

C        ...Computer name
         READ(IBOSTO,END=80) CPUNAM

C        ...Binary file type (fixed- or vbl-length)
         READ(IBOSTO,END=80) BFILTYP

C        ...Reset NBINF, depending on BFILTYP
         IF(BFILTYP.EQ.'C')THEN
            NBINF = 2
         ELSE
            NBINF = 1
         ENDIF

C        ...Number of files to read
         READ(IBOSTO,END=80) NUMFIL
         CALL QAMAXERR( .TRUE.,'NUMFIL','MAXFIL',NUMFIL,MAXFIL )

C        ...Update the number of QA records
C           (for BRAGFLO; done in main program for POSTBRAG)
C           add 2 even if the PREBRAG QA records were not on the binary file
C           I will keep track using the logical variable LQAPB
         NQAREC = NQAREC + 2

C              ...Assemble the QA record for program BRAGFLO
         GOTO 60

      ELSEIF( NBINF.EQ.0 )THEN
C        ...Find QA record in ASCII BRAGFLO output
         IREC = 0
   20    READ(IBOSTO,'(A)',END=80) STRING
         IREC = IREC + 1
         IF(STRING(1:10) .EQ. ' ** Beginn') THEN
C           ...Program name
            PRGRAM = STRING(18:24)
C           ...Program version number
            VRSION = STRING(42:45)
C           ...Program revision date
            REVDATE = STRING(65:72)
C           ...Read next line for time, date, and computer name
            READ(IBOSTO,'(A)',END=80) STRING
C           ...Find run date
            MNDAYR = STRING(16:23)
C           ...Time run initiated
            TIME = STRING(28:35)
c           ...Find computer name
            CPUNAM = STRING(47:78)
C           ...Update the number of QA records
            NQAREC = NQAREC + 2
            GOTO 50
         ELSE
C           ...Limit search to 1st 10 lines of ASCII output
            IF( IREC .GT. 10 )THEN
               WRITE(NOUTFL,*)' *** Cannot find QA record on ',
     $            'BRAGFLO output file. ***'
               WRITE(NOUTFL,*)' *** POSTBRAG will proceed ',
     $            'without adding QA info to CAMDAT file. ***'
C              ...Exit routine
               GOTO 70
            ELSE
C              ...Try next line of BRAGFLO output
               GOTO 20
            ENDIF
         ENDIF
C
C        ...Look for PREBRAG QA RECORDS on ASCII file
   50    READ(IBOSTO,'(A)',END=80) STRING
         IF(STRING(1:8) .EQ. '  **QA**')THEN
C        ...FOUND PREBRAG QA RECORD
            LQAPB = .TRUE.
C        ...READ NEXT 5 LINES FOR PREBRAG QA
C        ...Program name
            READ (IBOSTO, '(2X,A8)',END=80) PB_PRGRAM
C        ...Program version
            READ (IBOSTO, '(2X,A8)',END=80) PB_VRSION
C        ...Program revision date
            READ (IBOSTO, '(2X,A8)',END=80) PB_DATE
C        ...Run date
            READ (IBOSTO, '(2X,A8)',END=80) PB_MNDAYR
C        ...Run time
            READ (IBOSTO, '(2X,A8)',END=80) PB_TIME
            GO TO 50
         ELSEIF(STRING(1:15) .EQ. ' Number of file') THEN
            READ (STRING(34:37),'(I4)') NUMFIL

C           ...Assemble the QA record for program BRAGFLO
            GOTO 60
         ELSE
            GOTO 50
         ENDIF
      ENDIF

C     ...Assemble the PREBRAG AND BRAGFLO QA information
   60 CONTINUE
C     ...PREBRAG (only if PREBRAG QA records were found)
      IF(LQAPB)THEN
        QAINFO(1,NQAREC-1)(:8) = PB_PRGRAM(:8)
        QAINFO(2,NQAREC-1)(:8) = PB_VRSION(:8)
        QAINFO(3,NQAREC-1)(:8) = PB_MNDAYR(:8)
        QAINFO(4,NQAREC-1)(:8) = PB_TIME(:8)
      ENDIF
C     ...BRAGFLO
        QAINFO(1,NQAREC)(:8) = PRGRAM(:8)
        QAINFO(2,NQAREC)(:8) = VRSION(:8)
        QAINFO(3,NQAREC)(:8) = MNDAYR(:8)
        QAINFO(4,NQAREC)(:8) = TIME(:8)
        
   70 RETURN
   80 CALL QAABORT('(QABRAG) - EOF in output before all QA '//
     $   'info read ***')
C
C--------------------------------------------------------------------------
C**** End of SUBROUTINE QABRAG ****
C--------------------------------------------------------------------------
C
      END
