*DECK RBRAGO
      SUBROUTINE RBRAGO( LABELEL,  NAMELV2,  VALELV2,  VALGLV2, VALHIV2,
     $                   UNTCNVHI, UNTCNVGL, UNTCNVEL, TIME,    HIFLAG,
     $                   LEND,     IEBLOC,   NSUMM,    RVARS )
C***********************************************************************
C***                                                                 ***
C***           R_eads the B_R_A_G_FLO  O_utput file                  ***
C***           -          - - - -      -                             ***
C***********************************************************************
C
C  PURPOSE:     Reads the BRAGFLO output file and fills various
C               BRAGFLO output arrays.
C
C  AUTHOR:      James D. Schreiber
C
C  UPDATED:     19-MAY-1994  --JDS: Added a check on NHIEL; if NHIEL=0
C                                   (when there are no history vbls
C                                   based on element vbls), do not read
C                                   blank record from binary file, as
C                                   was being done.
C               08-NOV-1993  --JDS: Changed order of history variables,
C                                   making time-related variables first
C                                   instead of last.  Eliminated local
C                                   history variable names.  Changed
C                                   formats so all ASCII reads are as
C                                   reals.  BRAGFLO changed to write all
C                                   all history variables to binary out-
C                                   put files as reals (version 3.00VV).
C               27-OCT-1993  --JDS: Adapted to POSTBRAG for BRAGFLO
C                                   version >2.30VV. Numerous changes in
C                                   output file structure (esp. binary).
C                                   Deleted use of MAPOUT array.
C               02-AUG-1993  --JSR: Adapted to CAMDAT_LIB routines.
C                                   Adapted to 1-Dimensional arrays.
C                                   Adapted to POSTBRAG_T for BRAGFLO_T.
C                                   Cosmetic changes so that all
C                                   records <= 72 characters in length.
C               05-JAN-1993  --JDS: Deleted reading of NGVAR from
C                                   binary file, since it is not written
C                                   at each time step in this version of
C                                   BRAGFLO.
C                                   Modified reading of ASCII output
C                                   since number of global variables is
C                                   greatly reduced.
C               02-SEP-1992  --JDS: Deleted ISEVOK & NAMGLV from
C                                   argument list & from FORTRAN
C                                   declarations, since neither
C                                   was used.  Moved NSUMM
C                                   calculation from within ASCII
C                                   loop to end so number of time
C                                   steps is saved.
C               06-AUG-1992  --JDS: Added VALGV to argument list,
C                                   which is used as local variable
C                                   for reading in new global
C                                   variables; Added code for
C                                   reading NGVAR (no. of new
C                                   global variables) & values of
C                                   new globals from
C                                   binary & ASCII BRAGFLO files.
C               21-FEB-1991  --JDS: Added MAPOUT array;
C                                   Changed RDEL to REAL;
C                                   Added calls to GETDIS.
C               26-FEB-1992  --JDS: Fixed check for "Elapsed Time"
C                                   to Upper & Lower case;
C                                   Fixed finding mass balances
C                                   with DO 20 loop.
C               25-FEB-1992  --JDS: Added reading of mass balance
C                                   information.  Moved statement
C                                   label 10 down one line.
C               12-NOV-1991  --JDS: Added RVARS to argument list.
C               17-JUL-1991  --JDS: Finished converting to BRAGFLO.
C               09-MAY-1991  --JDS: Converted from BOAST to BRAGFLO.
C               01-MAY-1991  --JDS: Added parameter to call to RBSUMM.
C               30-APR-1991  --JDS: Changed Well summary data to
C                                   Global variables;
C                                   Modified call to RBWELL
C                                   & associated code.
C               26-FEB-1991  --JDS: Added NVEL;
C                                   Fixed test for completion of
C                                   reading all required element
C                                   variables.
C               21-SEP-1990  --JDS: Change BOAST z-velocity direction
C                                   to conform to CAMDAT
C                                   (z-velocity positive upward).
C               06-AUG-1990  --JDS: Added calls to RBWELL to get well
C                                   data; added ISEVOK to call list.
C               24-JAN-1990  --JDS: Original version.
C
C  CALLED BY:   PROCBO
C
C  CALLS:       GETDIS
C               QAABORT
C
C  ARGUMENTS:
C   ENTRY/
C    --common blocks
C     /BRAG/ (from FORTRAN statement: INCLUDE 'brag.inc')
C      NGVAR  = Number of BRAGFLO global variables (INTEGER)
C
C     /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C      IBOSTO = Device number of BRAGFLO output file.
C      NOUTFL = File unit of diagnostics/debug output file.
C
C     /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C      NELBLK = Number of Element Blocks (Regions) in mesh.
C
C     /NEWANA/ (from FORTRAN statement: 'newana.inc')
C      NVAREL2= Number of NEW CAMDAT ELement variables
C      NVARGL2= Number of NEW CAMDAT GLobal  variables
C      NVARHI2= Number of NEW CAMDAT HIstory variables
C
C     /UNTCNV/ (from FORTRAN statement: INCLUDE 'untcnv.inc')
C      BUNITS = BRAGFLO type units (CHARACTER*8)
C             = 'ENGLISH' or
C             = 'SI'
C
C    --through subroutine call
C      UNITS  = Units conversion system for BRAGFLO output
C               variables to CAMDAT (REAL)
C      IEBLOC = Global element number array.
C      NAMELV = Array of Element Variable Names.
C      RVARS  = REAL scratch array (memory allocated dynamically).
C
C   LOCAL/
C      CV     = CHARACTER string: one full line of BRAGFLO output.
C      I      = DO loop control parameter.
C      K      = DO loop control parameter.
C      KUIT   = E-O-F indicator from subroutines:
C             = 0; no EOF
C             = 1; EOF in READEL
C
C   EXIT/
C    --through subroutine call
C      HIFLAG = HIstory variable FLAG indicating whether history vbls
C               only or history vbls along with global vbls and element
C               vbls will be read in at this time step: (LOGICAL)
C                   KHIST          HIFLAG       WHOLE
C               (from BRAGFLO,   (in RBRAGO,   (in
C                in RBRAGO)       to WRCANA)   WRCANA)
C               --------------   -----------   -------
C                     0          .TRUE. =1.0   .FALSE.    Hist vbls only
C                     1          .FALSE.=0.0   .TRUE.     All vbls
C      LEND   = E-O-F indicator:
C             = 0; no EOF
C             = 1; normal end.
C      TIME   = Elapsed time read from BRAGFLO output [s].
C      VALELV2= CAMDAT ELement Variable values array (REAL)
C      VALGLV2= CAMDAT GLobal  Variable values array (REAL)
C      VALHIV2= CAMDAT HIstory Variable values array (REAL)
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'brag.inc'
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'newana.inc'
      INCLUDE 'untcnv.inc'
      INTEGER I, K, KHIST, KUIT, LEND, L1, L2, NSUMM
      INTEGER IEBLOC(*)
      LOGICAL HIFLAG  !apg was REAL
      DOUBLE PRECISION RVARS(*),   UNTCNVEL(*), UNTCNVGL(*),  !apg REAL
     &   UNTCNVHI(*),  !apg REAL
     $        VALGLV2(*), VALHIV2(*),  VALELV2(*),  TIME
      CHARACTER*(8)   NAMELV2(*)
      CHARACTER*(50)  LABELEL(*)
      CHARACTER*(132) CV
C
C<><><><><><><><><><><>
C...BEGIN PROCEDURES...
C<><><><><><><><><><><>
C
      KUIT = 0
      LEND = 0
      CV   = ' '
C
C**** Extract BRAGFLO History variables ****
C
      IF( NBINF.EQ.1 )THEN

C        ...Get elapsed time and first NVTIME (time-related) history
C           variables from variable-length-record binary BRAGFLO
C           output file (TIMEDAY,TIMEYR,DELT,DELTDAY,DELTYR,ITERAVG,
C           CPUS,CPUHR,STEPNUM,ITERTOT,KHIST)
         READ(IBOSTO,END=120)TIME,(VALHIV2(I),I=1,NVTIME),KHIST

C        ...Get rest of History variables
C        ...If there are no element-vbl-based history vbls, do NOT read
C           a blank record.
         IF (NHIEL .GT. 0) READ(IBOSTO,END=120) (VALHIV2(I),I=NVTIMP,
     1      NVARHI2)

      ELSEIF( NBINF.EQ.2 )THEN

C        ...Get time info from fixed-length binary BRAGFLO output file
         READ(IBOSTO,END=120)TIME,(VALHIV2(I),I=1,NVTIME),KHIST

C        ...Get rest of History variables
         DO 30 K=1,(NHIEL+127)/128
            L1 = (K-1)*128 + NVTIMP
            L2 = MIN(L1+127,NVARHI2)
            READ(IBOSTO,END=120) (VALHIV2(I),I=L1,L2)
   30    CONTINUE

      ELSEIF( NBINF.EQ.0 )THEN

C        ...Get elapsed time from ASCII BRAGFLO output file
   60    READ(IBOSTO,'(A)',END=120) CV
         IF( CV(5:16).EQ.'Elapsed Time' )THEN
            READ(IBOSTO,1010,END=120) TIME,(VALHIV2(I),I=1,2)
            READ(IBOSTO,'(A)',END=120) CV
            READ(IBOSTO,1010,END=120) (VALHIV2(I),I=3,5)
            READ(IBOSTO,1020,END=120) VALHIV2(6)
            READ(IBOSTO,1030,END=120) VALHIV2(7)
            READ(IBOSTO,1040,END=120) VALHIV2(8)
            READ(IBOSTO,1050,END=120) VALHIV2(9)
            READ(IBOSTO,1050,END=120) VALHIV2(10)

C           ...Set History variable flag, HIFLAG, to mean that this
C              time step has ALL variables (by default, in ASCII output)
            HIFLAG = .FALSE.  !apg was 0.0

C           ...Read down to mass balances
            DO 70 K=1,3
               READ(IBOSTO,'(A)',END=120) CV
   70       CONTINUE

C           ...Get mass balances
            READ(IBOSTO,1000,END=120)  VALGLV2(1)
            READ(IBOSTO,1000,END=120)  VALGLV2(2)
            READ(IBOSTO,'(A)',END=120) CV
            READ(IBOSTO,'(A)',END=120) CV
            READ(IBOSTO,1000,END=120)  VALGLV2(3),(VALGLV2(I),I=5,7)
            READ(IBOSTO,1000,END=120)  VALGLV2(4),(VALGLV2(I),I=8,10)

            DO 80 I=1,NVARGL2
               VALGLV2(I) = VALGLV2(I)*UNTCNVGL(I)
   80       CONTINUE
         ELSE
C           ...Read next BRAGFLO ASCII output record
            GOTO 60
         ENDIF

      ENDIF

C        ...Set History variable flag, HIFLAG
      IF(NBINF.GE.1)THEN
         IF(KHIST.EQ.1)THEN
            HIFLAG = .FALSE.  !apg was 0.0
         ELSE
            HIFLAG = .TRUE.  !apg was 1.0
         ENDIF
      ENDIF

C     ...Convert units for all history variables
      DO 10 I=1,NVARHI2
         VALHIV2(I) = VALHIV2(I)*UNTCNVHI(I)
  10  CONTINUE
C
C**** Extract BRAGFLO output element variable values ****
C
      IF(NBINF.EQ.0)THEN

C        ...Search ASCII BRAGFLO output for element variables
         DO 100 K=1,NVAREL2
   90       READ(IBOSTO,'(A)',END=130) CV
            IF( CV(2:38).EQ.LABELEL(K)(1:37) )THEN
C              ...Get BRAGFLO output element variable values
               CALL GETDIS( NAMELV2(K), NAMELV2,     VALELV2, IEBLOC,
     $                      KUIT,       UNTCNVEL(K), RVARS )
               IF( KUIT.EQ.1 )GOTO 130
            ELSE
C              ...Read next ASCII BRAGFLO output record
               GOTO 90
            ENDIF
 100     CONTINUE
         NSUMM = NSUMM + 1

      ELSEIF(NBINF.GT.0 .AND. KHIST.EQ.1)THEN

C        ...Get global and element vbls only if KHIST=1 (HIFLAG=FALSE)

C        ...First get mass balance information (global variables)
C           (currently same for variable & fixed length records
C           because NGVARGL2 is small [=10])
         READ(IBOSTO,END=120) (VALGLV2(I),I=1,NVARGL2)
         DO 20 I=1,NVARGL2
            VALGLV2(I) = VALGLV2(I)*UNTCNVGL(I)
  20     CONTINUE

C        ...Get BRAGFLO output element variable values
         DO 110 K=1,NVAREL2
            CALL GETDIS( NAMELV2(K), NAMELV2,     VALELV2, IEBLOC,
     $                   KUIT,       UNTCNVEL(K), RVARS )
            IF( KUIT.EQ.1 )GOTO 130
 110     CONTINUE
         NSUMM = NSUMM + 1
      ENDIF

C     ...Exit routine
      GOTO 140

C     ...END-OF-FILE found
  120 WRITE(NOUTFL,*)' *** [RBRAGO] E-O-F in RBRAGO, looking'//
     1   ' for start of next time step. ***'
      WRITE(NOUTFL,*)'              Processing will terminate'//
     1   ' normally.'
      LEND = 1
C     ...Exit routine
      GOTO 140

  130 IF( KUIT.EQ.0 )THEN
         WRITE(NOUTFL,*)' *** [RBRAGO] E-O-F READ in RBRAGO'//
     1      ' after start of a time step. ***'
         IF( NBINF.EQ.0 )THEN
            WRITE(NOUTFL,*) ' *** [RBRAGO] Last line read in RBRAGO:'
            WRITE(NOUTFL,'(A)') CV
         ENDIF
      ELSEIF( KUIT.EQ.1 )THEN
         WRITE(NOUTFL,*)' *** [RBRAGO] E-O-F READ in READEL'//
     1      ' after start of a time step. ***'
      ENDIF
      WRITE(NOUTFL,*)' *** [RBRAGO] Processing will terminate'//
     1   ' normally,'
      WRITE(NOUTFL,*)'              ignoring unfinished'//
     1   ' portion of last time step.'
      LEND = 1

  140 RETURN
C
C--------------  F O R M A T   S T A T E M E N T S  --------------------
C
 1000 FORMAT( 16X,E12.5,13X,3F5.0 )
 1010 FORMAT( 4X,E13.6,6X,E13.6,6X,E13.6 )
 1020 FORMAT( 23X,F6.0 )
 1030 FORMAT( 46X,F7.0 )
 1040 FORMAT( 46X,F7.3 )
 1050 FORMAT( 31X,F9.2 )
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE RBRAGO ****
C-----------------------------------------------------------------------
C
      END
