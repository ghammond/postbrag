*DECK RCTIM0
      SUBROUTINE RCTIM0( TIME,   HIFLAG )
!apg Removed Q,C
C***********************************************************************
C****                                                               ****
C****         R_eads "connected" C_amdat T_I_M_e  0 section         ****
C****         -                  -       - - -    -                 ****
C***********************************************************************
C
C  PURPOSE:    Reads "ANALYSIS" (time) section of the input
C              CAMDAT file into common blocks and arrays for the
C              first time-step.
C
C  AUTHOR:     Jonathan S. Rath
C
C  UPDATED:    16-SEP-1993  --JDS: No changes; now used in POSTBRAG for
C                                  BRAGFLO version >2.30VV.
C              05-AUG-1993  --JSR: First Ed. for POSTBRAG_T.
C
C  CALLED BY:  PROCDB
C
C  CALLS:      DBITIME
C              QAABORT
C
C  ARGUMENTS:
C  ENTRY/
C   --common blocks
C    /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C     IDB    = Device no of Input CAMDAT (INTEGER)
C
C    /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C     NELBLK = No. of materials or regions (INTEGER)
C     NSTEP  = No. of time steps (INTEGER)
C     NUMEL  = No. of elements (INTEGER)
C     NUMNOD = No. of nodes (INTEGER)
C     NVAREL = No. of ELEMENT type ANALYSIS variables
C     NVARGL = No. of GLOBAL  "                     "
C     NVARHI = No. of HISTORY "                     "
C     NVARND = No. of NODAL   "                     "
C
C   --through subroutine call
C
C  LOCAL/
C     IERR   = INTEGER error code
C
C  EXIT/
C   --through subroutine call
C     TIME   = Time value (REAL)
C     HIFLAG = History Flag (LOGICAL)
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INTEGER  IERR
      LOGICAL HIFLAG  !apg was REAL
      DOUBLE PRECISION TIME  !apg REAL
      LOGICAL WHOLE
C
C<><><><><><><><><><><>
C...Begin Procedures...
C<><><><><><><><><><><>
C
C     ...Read a time step
      CALL DBITIME( IDB, TIME, WHOLE, IERR )
      IF (IERR .EQ. -1) IERR = 0  !apg
      IF(WHOLE)THEN
         HIFLAG=.FALSE.  !apg was 0.0
      ELSEIF(.NOT.WHOLE)THEN
         HIFLAG=.TRUE.  !apg was 1.0
      ENDIF
      IF(IERR.NE.0)THEN
         WRITE(NOUTFL,*)
     $   ' **** ERROR reading input CAMDAT time values ****'
         WRITE(NOUTFL,*)'     TIME STEP  = 1'
         WRITE(NOUTFL,*)'     TIME VALUE = ',TIME
         WRITE(NOUTFL,*)'     Input CAMDAT has (NSTEP)=',
     $                  NSTEP,' time steps '
         CALL QAABORT(
     $   '(RCTIM0) - reading INPUT CAMDAT time values **')
      ENDIF
C     ...Do not read input CAMDAT HISTORY variable values
C     ...Do not read input CAMDAT GLOBAL  variable values
C     ...Do not read input CAMDAT NODAL   variable values
C     ...Do not read input CAMDAT ELEMENT variable values
      RETURN
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE RCTIM0 ****
C-----------------------------------------------------------------------
C
      END
