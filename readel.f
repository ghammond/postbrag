*DECK READEL
      SUBROUTINE READEL( IVAR, VALELV2, KUIT, IEBLOC, RVARS )
C***********************************************************************
C***                                                                 ***
C***  R_E_A_D_s the BRAGFLO output E_L_ement (Block) distributions   ***
C***  - - - -                      - -                               ***
C***********************************************************************
C
C  PURPOSE:     Reads the BRAGFLO output Pressure, Saturation, etc.
c               distributions, which are block-centered values and
C               therefore treated as Element variables in CAMDAT.
C
C  AUTHOR:      James D. Schreiber
C
C  UPDATED:     08-OCT-1993  --JDS: Deleted RDEL from argument list.
C                                   No longer used to read in
C                                   element variables as 3D arrays.
C                                   RVARS used exclusively, now.
C                                   Some cosmetic changes, adapting
C                                   to POSTBRAG for BRAGFLO
C                                   version >2.30VV.
C               02-AUG-1993  --JSR: Adapted to 1-Dimensional arrays.
C                                   Adapted to CAMDAT_LIB routines.
C                                   Adapted to POSTBRAG_T for BRAGFLO_T.
C                                   Cosmetic changes so that all
C                                   records <= 72 characters.
C               04-MAR-1992  --JDS: Fixed ordering of layer numbering
C                                   (binary output order opposite of
C                                   ASCII layer order).
C               26-FEB-1992  --JDS: Added reading of extra ASCII line
C                                   with Layer No.; added reading of
C                                   extra integer at beginning of each
C                                   J-index in ASCII.
C               25-FEB-1992  --JDS: Changed order of reading J & K
C                                   indices in in binary read, since
C                                   BRAGFLO coords now conform to
C                                   CAMCON; ASCII reads J-indices
C                                   changed, but K-indices still
C                                   reversed; When EOF found,
C                                   backspace, reread, & write
C                                   last line to debug output.
C               12-NOV-1991  --JDS: Added reading of binary output
C                                   written on Alliant (NBINF=2),
C                                   including array RVARS
C                                   and integers M, L1, & L2.
C               18-JUL-1991  --JDS: Finished converting to
C                                   BRAGFLO.
C               09-MAY-1991  --JDS: Started converting from
C                                   BOAST to BRAGFLO.
C               21-FEB-1991  --JDS: Changed RDEL to REAL.
C               11-APR-1990  --JDS: Replaced global element
C                                   search with IEBLOC.
C               29-JAN-1990  --JDS: Original version.
C
C  CALLED BY:   GETDIS
C
C  CALLS:       QAABORT
C
C  ARGUMENTS:
C   ENTRY/
C    --common blocks
C     /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C      IBOSTO = Device number of BRAGFLO ASCII output file
C      NOUTFL = File unit of diagnostics/debug output file
C      NBINF  = Indicates whether BRAGFLO output file is binary
C             = 0; output file is ASCII
C             = 1; output file is binary, with variable record lengths
C             = 2; output file is binary, with fixed record lengths
C
C     /GEOPAR/ (from FORTRAN statement: INCLUDE 'BF3_GEOPAR.INC)
C      MAXNELB= Maximum no. of CAMDAT element blocks (regions)
C      NX     = No. of elements in x-direction
C      NXY    = NX*NY
C      NY     = No. of elements in y-direction
C      NZ     = No. of elements in z-direction
C
C    --through subroutine call
C      IVAR   = Index of element variable no.
C      IEBLOC = Global element number array
C      RVARS  = REAL scratch array (memory allocated dynamically)
C
C   LOCAL/
C      CLINE  = CHARACTER scratch variable
C      II     = DO-loop control variable
C      JJ     = DO-loop control variable
C      KK     = DO-loop control variable
C      KL     = Layer number
C      L1     = INTEGER temporary variable
C      L2     = INTEGER temporary variable
C      NEL    = Global element number for
C               BRAGLFO block (II,JJ,KK)
C      NJK    = INTEGER temporary variable
C      NK     = INTEGER temporary variable
C      M      = INTEGER temporary variable
C
C   EXIT/
C    --through subroutine call
C      VALELV2 = CAMDAT Element variables array (REAL)
C               (for BRAGFLO variables: PRESEL, SATOILEL,
C                SATGASEL, PORVOLEL, etc.)
C      KUIT    = INTEGER end-of-file flag:
C              = 0; no EOF
C              = 1; EOF found in READEL
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INTEGER I,    ICAMEL,  IEND,  II,  IPOS,  ISTART, IVAR, JJ,
     $        KK,   KL,      KUIT,  L1,  L2,    NEL,    NJK,  NK,
     $        M
      INTEGER IEBLOC(*)
      DOUBLE PRECISION RVARS(*), VALELV2(*)  !apg REAL
      CHARACTER*(132) CLINE
C
C<><><><><><><><><><><>
C...BEGIN PROCEDURES...
C<><><><><><><><><><><>
C
      KUIT = 0
      IF( NBINF.EQ.1 )THEN
C        ...Read element variables using
C           binary BRAGFLO output file format: variable-length records
         READ(IBOSTO,END=100,ERR=90)(RVARS(I),I=1,NUMEL)

      ELSEIF( NBINF.EQ.2 )THEN
C        ...Read element variables using
C           binary BRAGFLO output file format: fixed-length records
         DO 10 M=1,(NUMEL+127)/128
            L1 = (M-1)*128+1
            L2 = MIN0(L1+127,NUMEL)
            READ(IBOSTO,END=100,ERR=90)(RVARS(II),II=L1,L2)
  10     CONTINUE
      ENDIF

C     ...Loop over layers (done for both binary & ASCII output)
      DO 70 KK=1,NZ
         IF( NBINF .EQ. 0 )THEN
C           ...If ASCII output, read the line with "Layer #"
            READ(IBOSTO,'(A)',END=100,ERR=90)CLINE
C           ...Reverse the order -- layer NZ 1st in ASCII output
            KL = NZ - KK + 1
         ELSE
            KL = KK
         ENDIF

         NK = (KL-1)*NXY

         DO 60 JJ=1,NY
            IF( NBINF .EQ. 0)THEN
C              ...ASCII data must be read now
C              ...Read element variables using
C                 ASCII BRAGFLO output file format
               ISTART = (KL-1)*NXY+(JJ-1)*NX+1
               IEND   = ISTART+NX-1
               READ(IBOSTO,*,END=100,ERR=90)
     $         M,(RVARS(II),II=ISTART,IEND)
            ENDIF

            NJK = (JJ-1)*NX + NK

            DO 50 II=1,NX
C              ...Get global element number for
C                 BRAGFLO block (II,JJ,KK)
               NEL           = NJK + II
C              ...Get CAMDAT global element number from
C                 BRAGFLO FD numbering scheme.
               ICAMEL        = IEBLOC(2*NEL-1)
C              ...Get CAMDAT ATTRIB(*) array position
               IPOS          = (IVAR-1)*NUMEL+ICAMEL
C              ...Assign CAMDAT element variable value
               VALELV2(IPOS) = RVARS(NEL)
  50        CONTINUE

  60     CONTINUE
  70  CONTINUE

C     ...Exit routine
      GOTO 999

C     ...ERROR detected
  90  WRITE(NOUTFL,*)
     $' *** ERROR reading BRAGFLO output element variables ***'
      CALL QAABORT('(READEL) - reading BRAGFLO output element'//
     $             ' variables ***')

C     ...END-OF-FILE found
 100  BACKSPACE(IBOSTO)
      WRITE(NOUTFL,*) ' *** E-O-F READ IN READEL ***'
      IF( NBINF .EQ. 0 )THEN
         READ (IBOSTO,'(A)') CLINE
         WRITE(NOUTFL,*) ' *** Last line read in READEL:'
         WRITE(NOUTFL,'(A)') CLINE
      ENDIF
      KUIT = 1
 999  RETURN
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE READEL ****
C-----------------------------------------------------------------------
C
      END
