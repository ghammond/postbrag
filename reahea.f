*DECK REAHEA
      SUBROUTINE REAHEA( COORD,  MAP,    NUMELB,
     $                   NELNOD, NUMPRP, NATRIB, IASPRP, IASATR,
     $                   ICONN,  XMATPR, ATTRIB, NAMELB, NMATPR,
     $                   NAMATR, NAMECO, INFO,   QAINFO )
!apg Removed Q,C
C***********************************************************************
C****                                                               ****
C****          R_E_A_d  camdat  H_E_A_der  section  routine         ****
C****          - - -            - - -                               ****
C***********************************************************************
C
C  PURPOSE:    Reads pertinent "HEADER" sections of the input
C              CAMDAT file into common blocks and arrays
C
C  AUTHOR:     Jonathan S. Rath
C
C  UPDATED:    16-SEP-1993  --JDS: No changes; now used in POSTBRAG for
C                                  BRAGFLO version >2.30VV.
C              05-AUG-1993  --JSR: First Ed. for POSTBRAG_T.
C
C  CALLED BY:  PROCDB
C
C  CALLS:      DBITITLE
C              DBIMAP
C              DBIELBLK
C              DBILINK
C              DBIXYZ
C              DBIPRNM
C              DBIPROP
C              DBIATTR
C              DBIATNAM
C              DBIINFO
C              DBIQAREC
C              DBINSTEP
C              QAABORT
C
C  ARGUMENTS:
C  ENTRY/
C   --common blocks
C    /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C     IDB    = Device no of Input CAMDAT File
C
C    /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C     NDIM   = Number of physical DIMensions
C     NELBLK = Number of ELement BLocKs in mesh
C     NQAREC = Number of QA records
C     NUMEL  = NUmber of ELeMents in mesh
C     NUMNOD = NUMber of NODes in mesh
C     NUQPRP = NUmber of Unique PRoPerty names
C     NUQATR = NUmber of Unique ATtRibute names
C     NVAREL = Number of ELEMENT type ANALYSIS variables
C     NVARGL = Number of GLOBAL  "                     "
C     NVARHI = Number of HISTORY "                     "
C     NVARND = Number of NODAL   "                     "
C     NXINFO = Number of information records
C
C  LOCAL/
C     IERR   = INTEGER error code
C
C  EXIT/
C   --common block
C    /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C     NSTEP  = No. of time steps in input CAMDAT (INTEGER)
C     HEADER = CAMDAT problem description (CHARACTER*80)
C
C   --through subroutine call
C     COORD  = Nodal point coordinates array
C     MAP    = Optimized element array
C     NUMELB = No. of elements in each element block array
C     NELNOD = No. of nodes per element in material array (INTEGER)
C     IASPRP = LOGICAL property names array
C     IASATR = Logical array for attributes in materials
C     NUMPRP = No. of property in each material array (INTEGER)
C     NATRIB = No. of attributes in each material array (INTEGER)
C     ICONN  = Element-node connectivity array
C     XMATPR = Propery values array (REAL)
C     ATTRIB = Attribute values array (REAL)
C     ISEVOK = Element variables output to DB truth table LOGICAL array
C     NAMELB = Names of the materials (CHAR*8)
C     NMATPR = Names of unique properties (CHAR*8)
C     NAMATR = Names of the attributes (CHAR*8)
C     NAMECO = Names of the physical coordinates array (CHAR*8)
C     INFO   = Additional/Optional information array (CHAR*80)
C     QAINFO = Quality Assurance array (CHAR*8)
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INTEGER I, IERR, J, K, NHISTP
      INTEGER IDUM
      INTEGER ICONN(*), MAP(*), NATRIB(*), NELNOD(*), NUMELB(*),
     $        NUMPRP(*)
      DOUBLE PRECISION ATTRIB(*), COORD(*), X(1), XMATPR(*)  !apg REAL
      CHARACTER*(8) NAMATR(*), NAMECO(*), NAMELB(*), NMATPR(*),
     $              QAINFO(4,*)
      CHARACTER*(80) INFO(*)
      LOGICAL IASATR(*), IASPRP(*)
C
C<><><><><><><><><><><>
C...Begin Procedures...
C<><><><><><><><><><><>
C
C     ...Read/set problem title (common /GEOPAR/ HEADER variable)
      CALL DBITITLE( IDB, HEADER, IERR )
      IF(IERR.NE.0)CALL QAABORT(
     $'(REAHEA) - reading CAMDAT title ***')

C     ...Read/set Coordinates array
      IF(NDIM.EQ.1)THEN
         CALL DBIXYZ( IDB,    NDIM,   NUMNOD, NAMECO, COORD,
     $                X,      X,      IERR )
      ELSEIF(NDIM.EQ.2)THEN
         CALL DBIXYZ( IDB,    NDIM,   NUMNOD, NAMECO, COORD,
     $                COORD(NUMNOD+1), X,      IERR )
      ELSEIF(NDIM.EQ.3)THEN
         CALL DBIXYZ( IDB,    NDIM,   NUMNOD, NAMECO, COORD,
     $                COORD(NUMNOD+1), COORD(2*NUMNOD+1), IERR )
      ENDIF
      IF(IERR.NE.0)
     $CALL QAABORT('(REAHEA) - reading COORDINATE values ***')

C     ...Read map array
      CALL DBIMAP( IDB, NUMEL, MAP, IERR )
      IF(IERR.NE.0)
     $CALL QAABORT('(REAHEA) - reading MAP array ***')

C     ...Read material ID and names information
      CALL DBIELBLK( IDB, NELBLK, NAMELB, NUMELB, IERR )
      IF(IERR.NE.0)
     $CALL QAABORT('(REAHEA) - reading material ID and names array ***')
C     ...Zero out NUMELB array (contains the material ID numbers)
C     ...This must be done not to confuse DBILINK.
      DO 10 I = 1,NELBLK
         NUMELB(I) = 0
  10  CONTINUE

C     ...Read connectivity data
      CALL DBILINK( IDB,    NELBLK,  NX,     NY,    NZ,
     $              NUMELB, MAXNENO, NELNOD, ICONN, IERR  )
      IF(IERR.NE.0)
     $CALL QAABORT('(REAHEA) - reading CONNECTIVITY array ***')

C     ...Read property names
      CALL DBIPRNAM( IDB, NUQPRP, NMATPR, IDUM, IDUM, IERR )
      IF(IERR.NE.0)
     $CALL QAABORT('(REAHEA) - reading PROPERTY NAMES array ***')

C     ...Read property data
      DO 20 I=1,NUQPRP
         J = (I-1)*NELBLK+1
         CALL DBIPROP( IDB, '*', I, IASPRP(J), XMATPR(J),
     $                 IDUM, IDUM, IERR )
         IF(IERR.NE.0)
     $   CALL QAABORT('(REAHEA) - reading PROPERTY values ***')
  20  CONTINUE

C     ...Construct the number of properties per material array
      DO 40 I=1,NELBLK
         NUMPRP(I) = 0
         DO 30 J=1,NUQPRP
            K = (J-1)*NELBLK+I
            IF(IASPRP(K))NUMPRP(I) = NUMPRP(I)+1
  30     CONTINUE
  40  CONTINUE

C     ...Read attribute names
      CALL DBIATNAM( IDB, NUQATR, NAMATR, IDUM, IDUM, IERR )
      IF(IERR.NE.0)CALL QAABORT(
     $'(REAHEA) - reading ATTRIBUTE NAMES array ***')

C     ...Read attribute data
      DO 50 I=1,NUQATR
         J = (I-1)*NELBLK+1
         K = (I-1)*NUMEL+1
         CALL DBIATTR( IDB, '*', I, IASATR(J), ATTRIB(K),
     $                 IDUM, IDUM, IERR )
         IF(IERR.NE.0)CALL QAABORT(
     $   '(REAHEA) - reading ATTRIBUTE values ***')
  50  CONTINUE

C     ...Construct the number of attributes per material array
      DO 70 I=1,NELBLK
         NATRIB(I) = 0
         DO 60 J=1,NUQATR
            K = (J-1)*NELBLK+I
            IF(IASATR(K))NATRIB(I) = NATRIB(I)+1
  60     CONTINUE
  70  CONTINUE

C     ...Read the INFOrmation records
      CALL DBIINFO( IDB, NXINFO, INFO, IERR )
      IF(IERR.NE.0)CALL QAABORT('(REAHEA) - reading INFO array ***')

C     ...Read QA data
      CALL DBIQAREC( IDB, NQAREC, QAINFO, IERR )
      IF(IERR.NE.0)CALL QAABORT('(REAHEA) - reading QA array ***')

C     ...Do not read TRUTH table for element variables
C     ...Do not read ANALYSIS variable names

C     ...Get the number of time steps (NSTEP)
      CALL DBINSTEP( IDB, NSTEP, NHISTP, IERR )
      IF(IERR.NE.0)CALL QAABORT('(REAHEA) - obtaining input CAMDAT '//
     $                          'no. of time steps ***')
      RETURN
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE REAHEA *****
C-----------------------------------------------------------------------
C
      END
