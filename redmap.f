*DECK REDMAP
      SUBROUTINE REDMAP( NGBHIV,   IJKHIV,   NAMELV2,  NAMGLV2,
     $                   NAMHIV2,  UNTCNVEL, UNTCNVGL, UNTCNVHI,
     $                   LABELEL,  LABELGL,  LABELHI,  LABUNTEL,
     $                   LABUNTGL, LABUNTHI )
C***********************************************************************
C***                                                                ****
C***      R_E_a_D  BRAGFLO  output  control  M_A_P_s  module        ****
C***      - -   -                            _ _ _                  ****
C***********************************************************************
C
C   PURPOSE:     Reads the BRAGFLO output file for element, global,
C                and history variable names, descriptive labels, and
C                units conversion factors to convert from BRAGFLO out-
C                put units to SI units.
C
C   AUTHOR:      James D. Schreiber
C
C   UPDATED:     10-JUN-1994  --JDS: Deleted section that tried to read
C                                    ASCII output history variables,
C                                    because ASCII output has NO history
C                                    variables.
C                08-NOV-1993  --JDS: Deleted BLANK10 variable; labels
C                                    are now written as character*50
C                                    variables in BRAGFLO.
C                27-OCT-1993  --JSR: Added a dummy character*10
C                                    variable, BLANK10, which is
C                                    read between variable labels &
C                                    units.  Can be deleted, if BRAGFLO
C                                    writes a variable label as a
C                                    character*50 field instead of
C                                    the current character*60 field.
C                20-SEP-1993  --JDS: Adapted to BRAGFLO version >2.30VV.
C                                    New BRAGFLO binary file structure:
C                                    includes names of all element,
C                                    global, and history vbl names along
C                                    with descriptive labels and conver-
C                                    sion factors.  Reading of NX, NY, &
C                                    NZ and BUNITS moved to QABRAG.
C                04-AUG-1993  --JSR: Adapted for BRAGFLO_T and for
C                                    use with new CAMCON_LIB and
C                                    CAMDAT_LIB routines.  Split
C                                    routine into two parts, so
C                                    that arrays can be allocated
C                                    memory exactly.
C                28-AUG-1992  --JDS: REDMAP is now called after QABRAG,
C                                    so the file has been read through
C                                    the run title (record #11 in binary
C                                    file); REDMAP starts reading from
C                                    there; reading of 1st 11 records
C                                    of binary file deleted; now reads
C                                    ASCII file down to 1st
C                                    distribution output (previously
C                                    done in QABRAG).
C                04-AUG-1992  --JDS: Added reading of global variable
C                                    number & names, NGVAR and GVNAM
C                                    (an array).
C                26-FEB-1992  --JDS: Fixed reading of NX,NY,NZ;
C                                    Fixed check for
C                                    "Variable Output Flags".
C                25-FEB-1992 - -JDS: Added reading of NZ & deleted
C                                    default NZ=1;
C                                    Added 2 more file names to be
C                                    read (total=6); fixed length of
C                                    file names to 130.
C                12-JUL-1991  --JDS: Moved argument list into UNTCNV.INC
C                11-JUL-1991  --JDS: Also reads NX & NY; reads through
C                                    top part of output to get to NX and
C                                    NY (formerly done in SCANBO).
C                                    Added GEOPAR.INC.
C                10-JUL-1991  --JDS: Changed MAPOUT array dimension
C                                    to *.
C                                    Added NMAP to argument list.
C                                    Revised format for reading
C                                    MAPOUT from ASCII output.
C                10-MAY-1991  --JDS: Converted from BOAST to BRAGFLO.
C                20-FEB-1991  --JDS: Original version.
C
C   CALLED BY:   PROCDB
C
C   CALLS:       QAABORT
C
C   ARGUMENTS:
C   ENTRY/
C     --common blocks
C      /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C       IBOSTO = Device number of output BRAGFLO file.
C       NOUTFL = Device number of diagnostics/debug output file.
C
C      /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C       NX     = INTEGER variable, number of elements in x-direction.
C       NY     = INTEGER variable, number of elements in y-direction.
C       NZ     = INTEGER variable, number of elements in z-direction.
C
C      /UNTCNV/ (from FORTRAN statement: INCLUDE 'untcnv.inc')
C       BUNITS = BRAGFLO type units (CHARACTER*8)
C              = 'ENGLISH' or
C              = 'SI'
C
C   LOCAL/
C       I      = Implied DO loop control variable.
C       J      = Implied DO loop control variable.
C       L      = Dummy INTEGER variable.
C       L1     = Dummy INTEGER variable.
C       L2     = Dummy INTEGER variable.
C       L3     = Dummy INTEGER variable.
C       LINE   = Dummy CHARACTER variable.
C       NREC   = Number of fixed-length binary records to read.
C
C   EXIT/
C     --common blocks
C      /BRAG/ (from FORTRAN statement: INCLUDE 'brag.inc')
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'brag.inc'
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'newana.inc'
      INCLUDE 'untcnv.inc'
      INTEGER  I, J, L, LH, L1, L2, L3, NREC
      INTEGER  NGBHIV(*), IJKHIV(*)
      DOUBLE PRECISION UNTCNVEL(*), UNTCNVGL(*), UNTCNVHI(*)  !apg REAL
      CHARACTER*(8)   NAMELV2(*),  NAMGLV2(*),  NAMHIV2(*)
      CHARACTER*(20)  LABUNTEL(*), LABUNTGL(*), LABUNTHI(*)
      CHARACTER*(50)  LABELEL(*),  LABELGL(*),  LABELHI(*)
      CHARACTER*(132) LINE
C
C<><><><><><><><><><><>
C...Begin Procedures...
C<><><><><><><><><><><>
C
C
      IF(NBINF.GT.0)THEN

C        ...Read BINARY BRAGFLO output file

C        ...History variable (I,J,K) locations [Rec #19]

         IF(NBINF.EQ.1)THEN
C           ...Variable-length records
            LH = 1
            DO 10 I=1,NHIV
               READ (IBOSTO,END=140)
     $            L,NGBHIV(I),(IJKHIV(J),J=LH,3*NGBHIV(I)+LH-1)
               LH = LH + 3*NGBHIV(I)
  10        CONTINUE
C           ...(LH should now be same as 3*MHIV)
         ELSE
C           ...Fixed-length records
C              (512 bytes, or 128 INTEGER*4 words, each)
            LH = 1
            DO 30 I=1,NHIV
               READ (IBOSTO,END=140) L,NGBHIV(I),(IJKHIV(J),
     $            J=LH,MIN(3*NGBHIV(I)+LH-1,126+LH-1))
               LH   = MIN((LH+3*NGBHIV(I)),(126+LH))
               L3   = LH + 3*NGBHIV(I) - 1
               NREC = (3*NGBHIV(I)+2+127)/128 - 1
               DO 20 J=1,NREC
                  L1 = LH
                  L2 = MIN(L1+127,L3)
                  READ (IBOSTO,END=140) (IJKHIV(L),L=L1,L2)
                  LH = L2 + 1
   20          CONTINUE
   30       CONTINUE
         ENDIF

C        ...Names, descriptive labels, units labels, & units conversion
C           factors for element variables [Rec #20]
         DO 40 I=1,NVAREL2
            READ(IBOSTO,END=140)L,NAMELV2(I),LABELEL(I),
     $                          LABUNTEL(I),UNTCNVEL(I)
  40     CONTINUE

C        ...Names, descriptive labels, units labels, & units conversion
C           factors for history variables [Rec #21]
         DO 50 I=1,NVARHI2
            READ(IBOSTO,END=140)L,NAMHIV2(I),LABELHI(I),
     $                          LABUNTHI(I),UNTCNVHI(I)
  50     CONTINUE

C        ...Names, descriptive labels, units labels, & units conversion
C           factors for global variables [Rec #22]
         DO 60 I=1,NVARGL2
            READ(IBOSTO,END=140)L,NAMGLV2(I),LABELGL(I),
     $                          LABUNTGL(I),UNTCNVGL(I)
  60     CONTINUE
C        ...Exit routine
         GOTO 150

      ELSE
C        ...Read ASCII BRAGFLO output file


C        ...BRAGFLO ASCII output should now be positioned to
C           read the Element variable names, labels, etc.
         DO 70 I=1,4
            READ(IBOSTO,'(A132)',END=140) LINE
  70     CONTINUE
         DO 80 I=1,NVAREL2
            READ(IBOSTO,1000,END=140)L,NAMELV2(I),LABELEL(I),
     $                               LABUNTEL(I),UNTCNVEL(I)
  80     CONTINUE

C        ...NOTE:  BRAGFLO ASCII output has NO History variables.

C        ...BRAGFLO ASCII output should now be positioned to
C           read the Global variable names, labels, etc.
         DO 110 I=1,4
            READ(IBOSTO,'(A132)',END=140) LINE
 110     CONTINUE
         DO 120 I=1,NVARGL2
            READ(IBOSTO,1000,END=140)L,NAMGLV2(I),LABELGL(I),
     $                               LABUNTGL(I),UNTCNVGL(I)
 120     CONTINUE

 130     READ(IBOSTO,'(A132)',END=140) LINE
         IF(LINE(1:22) .EQ. ' Grid block dimensions')THEN
C           ...Pointer positioned for GETATT.
C           ...Exit routine
            GOTO 150
         ELSE
C           ...Read next record of BRAGFLO output
            GOTO 130
         ENDIF
      ENDIF
C
 140  WRITE(NOUTFL,*) '** E-O-F found in BRAGFLO output file.'
      CALL QAABORT('(REDMAP1) - Premature EOF found in BRAGFLO'//
     $   ' output file ***')

 150  RETURN
C
C--------------  F O R M A T   S T A T E M E N T S  --------------------
C
1000  FORMAT(1X,I4,2X,A,2X,A,2X,A,1PE14.6)
C
C-----------------------------------------------------------------------
C**** End of SUBOUTINE REDMAP ****
C-----------------------------------------------------------------------
C
      END
