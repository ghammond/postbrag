*DECK SETNAM
      SUBROUTINE SETNAM( ISEVOK2 )
C***********************************************************************
C****                                                               ****
C****     S_E_T  N_A_Mes                                            ****
C****     - - -  - - -                                              ****
C***********************************************************************
C
C  PURPOSE:     Sets CAMDAT element variable truth
C               table ISEVOK2 (for BRAGFLO variables).
C
C  AUTHOR:      James D. Schreiber
C
C  UPDATED:     08-NOV-1993  --JDS: Deleted naming of history variables,
C                                   which are now read from BRAGFLO out-
C                                   put in REDMAP.
C               20-SEP-1993  --JDS: Adapted to POSTBRAG for BRAGFLO.
C                                   Deleted naming of global variables;
C                                   instead just add global variable
C                                   names read from BRAGFLO output
C                                   file. Add names of 10
C                                   additional history variables to
C                                   CAMDAT.
C               05-AUG-1993  --JSR: Adapted to CAMDAT_LIB.  Fully
C                                   adapted to POSTBRAG_T for
C                                   BRAGFLO_T.
C                                   Made ALL ARRAYS 1-DIMENSIONAL !
C                                   Added error handling w.r.t.
C                                   input CAMDAT NAMELV(*) array.
C                                   Made cosmetic changes so that
C                                   all records <= 72 characters.
C               05-AUG-1992  --JDS: Added COMMON/GVARNAM/GVNAM(109),
C                                   NGVAR; this allows global variable
C                                   names read in from BRAGFLO_T
C                                   output file in REDMAP to be added
C                                   to list of global variable names,
C                                   NAMGLV.
C               25-FEB-1992  --JDS: Added names of global variables,
C                                   new to BRAGFLO_T, relating to
C                                   mass balances.
C               12-JUL-1991  --JDS: Moved MAPOUT, NMAP, & NAMES from
C                                   argument list to UNTCNV.INC;
C                                   setting NAMES moved to OUTSET.
C               11-JUL-1991  --JDS: Deleted use of NWAT;
C                                   set dimension of MAPOUT to *;
C                                   added many Element variable names;
C                                   finished converting to BRAGFLO.
C                                   Added array NAMES to simplify
C                                   initializing NAMELV.
C               10-MAY-1991  --JDS: Adapted to BRAGFLO (partially).
C               30-APR-1991  --JDS: Changed Well rate & cumulative
C                                   output from Element to Global
C                                   variable.  Check MAPOUT array
C                                   for any Water output;
C                                   if none, use no Water Wells.
C               21-FEB-1991  --JDS: Added MAPOUT array.
C               03-AUG-1990  --JDS: Added 12 more Element variables
C                                   (WROILELx, etc.).
C               01-AUG-1990  --JDS: Added 4 Element variables &
C                                   3*(NELBLK+1) gas content Global
C                                   variables (FGASxxxx, etc.).
C               26-JAN-1990  --JDS: Original version.
C
C  CALLED BY:   PROCBO
C
C  ARGUMENTS:
C   ENTRY/
C    --common blocks
C     /FILEUN/ (from FORTRAN statement: INCLUDE 'BF3_FILEUN.INC')
C      NOUTFL  = Device no. of diagnostics/debug output file.
C
C     /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C      NELBLK  = Number of ELement BLocKs in mesh
C
C     /NEWANA/ (from FORTRAN statement: INCLUDE 'newana.inc')
C      NVAREL2 = Number of element variables
C
C   LOCAL/
C      I       = DO loop control parameter.
C
C   EXIT/
C    --through subroutine call
C      ISEVOK2 = New CAMDAT Element variables output
C                truth table array (LOGICAL)
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'brag.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'newana.inc'
      INTEGER I
      LOGICAL ISEVOK2(*)
C
C<><><><><><><><><><><>
C...Begin Procedures...
C<><><><><><><><><><><>
C
C     ...Add BRAGFLO output element variables truth table to CAMDAT
      DO 10 I=1,NELBLK*NVAREL2
         ISEVOK2(I) = .TRUE.
  10  CONTINUE

      RETURN
C-----------------------------------------------------------------------
C**** End of SUBROUTINE SETNAM *****
C-----------------------------------------------------------------------
      END
