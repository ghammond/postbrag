*DECK SIZES
      SUBROUTINE SIZES ( FILNAM )
C***********************************************************************
C***                                                                ****
C***      Read various S_I_Z_E_S in BRAGFLO output                  ****
C***                   - - - - -                                    ****
C***********************************************************************
C
C   PURPOSE:     Scans the BRAGFLO output file for sizes of various
C                output arrays that BRAGFLO has printed to the output
C                file.  Also reads file names, which get put into
C                info records.
C
C   AUTHOR:      James D. Schreiber
C
C   UPDATED:     10-JAN-1996  --JEB: Fixed bug in ASCII section in determining
C                                    BRAGFLO output units
C                10-JUN-1994  --JDS: Changed internal writes to internal
C                                    reads to convert character data to
C                                    NVAREL2 and NVARGL2.  In ASCII out-
C                                    put, changed NVTIME from 10 to 0,
C                                    since there are no history vbls of
C                                    any kind in ASCII output.
C                25-OCT-1993  --JDS: Original version.
C
C   CALLED BY:   PROCDB
C
C   CALLS:       QAABORT
C
C   ARGUMENTS:
C   ENTRY/
C     --common blocks
C      /BRAG/   (from FORTRAN statement: INCLUDE 'brag.inc')
C       NVTIME = Number of history variables read from same line as
C                time.  (Currently = 10).
C
C      /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C       IBOSTO = Device number of output BRAGFLO file.
C       NOUTFL = Device number of diagnostics/debug output file.
C
C      /GEOPAR/ (from FORTRAN statement: INCLUDE 'geopar.inc')
C       NX     = INTEGER variable, number of elements in x-direction.
C       NY     = INTEGER variable, number of elements in y-direction.
C       NZ     = INTEGER variable, number of elements in z-direction.
C
C      /UNTCNV/ (from FORTRAN statement: INCLUDE 'untcnv.inc')
C       BUNITS = BRAGFLO type units (CHARACTER*8)
C              = 'ENGLISH' or
C              = 'SI'
C
C   LOCAL/
C       I      = Implied DO loop control vbl.
C       J      = Implied DO loop control vbl.
C       L      = Dummy INTEGER variable.
C       L1     = Dummy INTEGER variable.
C       L2     = Dummy INTEGER variable.
C       L3     = Dummy INTEGER variable.
C       LINE   = Dummy CHARACTER variable.
C       NREC   = Number of fixed-length binary records to read.
C
C   EXIT/
C     --common blocks
C      /BRAG/ (from FORTRAN statement: INCLUDE 'brag.inc')
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'brag.inc'
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'newana.inc'
      INCLUDE 'untcnv.inc'
      INTEGER  I, NDTFIX
      CHARACTER*(80)  FILNAM(*)
      CHARACTER*(132) LINE
C
C<><><><><><><><><><><>
C...Begin Procedures...
C<><><><><><><><><><><>
C
C
      IF(NBINF.GT.0)THEN

C        ...Read BINARY BRAGFLO output file

C        ...File names
         DO 10 I=1,NUMFIL
            READ(IBOSTO,END=200) FILNAM(I)
  10     CONTINUE

C        ...Title of Run
         READ(IBOSTO,END=200) LINE

C        ...Grid sizes
         READ(IBOSTO,END=200) NX,NY,NZ

C        ...BRAGFLO output units
         READ(IBOSTO,END=200) BUNITS

C        ...Number of Element, History, and Global variables
         READ(IBOSTO,END=200) NVAREL2,NVTIME,NHIEL,NHIV,NVARGL2

C        ...First NVTIME (=10) history variables are time-related:
C           TIME (day & year), DELT (second, day, year), average
C           iterations, CPU (seconds & hours), step no., total
C           iterations.
C           Number of history variables that are derived from
C           element variables is NHIEL; starting point for additional
C           variables is NVTIMP.
C           Total number of history variables is NVARHI2.
         NVTIMP  = NVTIME + 1
         NVARHI2 = NVTIME + NHIEL

C        ...Exit routine
         GOTO 210

      ELSE
C        ...Read ASCII BRAGFLO output file

C        ...Should be positioned to read file names
         DO 90 I=1,NUMFIL
            READ(IBOSTO,'(A)',END=200) LINE
            READ(IBOSTO,'(A)',END=200) LINE
            READ(IBOSTO,'(A)',END=200) LINE
            READ(IBOSTO,'(A)',END=200) FILNAM(I)
            READ(IBOSTO,'(A)',END=200) LINE
   90    CONTINUE

C        ...Get simulation title (4th LINE)
         READ(IBOSTO,'(A)',END=200) LINE
         READ(IBOSTO,'(A)',END=200) LINE
         READ(IBOSTO,'(A)',END=200) LINE
         READ(IBOSTO,'(A)',END=200) LINE

C        ...Get size parameters
         READ(IBOSTO,'(A)',END=200) LINE
         READ(IBOSTO,'(A)',END=200) LINE
         READ(IBOSTO,*) NX,NY,NZ

C        ...Get units used in BRAGFLO output
         DO 100 I=1,7
            READ(IBOSTO,'(A)',END=200) LINE
  100    CONTINUE
         READ(IBOSTO,'(A,I5)',END=200) LINE(1:45),NDTFIX
         DO 110 I=1,3
            READ(IBOSTO,'(A)',END=200) LINE
  110    CONTINUE
         DO 120 I=1,NDTFIX
            READ(IBOSTO,'(A)',END=200) LINE
  120    CONTINUE
         DO 130 I=1,6
            READ(IBOSTO,'(A)',END=200) LINE
  130    CONTINUE
         BUNITS = LINE(6:13)

C        ...Find numbers of element & global variables
C           (Note:  No history variables of any kind in ASCII output)
  140    READ(IBOSTO,'(A)',END=200) LINE
         IF(LINE(3:29).EQ.'Number of element variables')THEN
            READ(LINE(56:60),'(I5)') NVAREL2
            READ(IBOSTO,'(A)',END=200) LINE
            READ(IBOSTO,'(A)',END=200) LINE
            READ(LINE(56:60),'(I5)') NVARGL2
            NHIV    = 0
            NHIEL   = 0
            NVTIME  = 0
            NVTIMP  = NVTIME + 1
            NVARHI2 = NVTIME
         ELSE
            GOTO 140
         ENDIF

C        ...Exit routine
         GOTO 210

      ENDIF
C
  200 WRITE(NOUTFL,*) '** E-O-F found in BRAGFLO output file.'
      CALL QAABORT('(REDMAP1) - Premature EOF found in BRAGFLO'//
     $   ' output file ***')

  210 RETURN
C
C-----------------------------------------------------------------------
C**** End of SUBOUTINE SIZES ****
C-----------------------------------------------------------------------
C
      END
