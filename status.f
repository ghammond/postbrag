*DECK STATUS
      SUBROUTINE STATUS( M )
C***********************************************************************
C
C  PURPOSE:    States program execution status and increases
C              fragmentation of the code.
C
C  AUTHOR:     Jonathan S. Rath
C
C  UPDATED:    15-SEP-1993  --JDS: No changes; now used in POSTBRAG for
C                                  BRAGFLO version >2.30VV.
C              05-AUG-1993  --JSR: Original version for POSTBRAG_T.
C
C  CALLED BY:  many
C
C  ARGUMENTS:
C  ENTRY/
C   --common blocks
C    /FILEUN/ (from FORTRAN statement: INCLUDE 'fileun.inc')
C     NOUTFL = Device no. of diagnostics/debug output file (INTEGER)
C
C   --through subroutine call
C     M      = Current execution subroutine completed (CHARACTER*(*))
C
C  LOCAL/
C     none
C
C  EXIT/
C     none
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'fileun.inc'
      CHARACTER*(*) M
      WRITE(NOUTFL,900)M
      RETURN
C
C---------------  F O R M A T    S T A T E M E N T S -------------------
C
 900  FORMAT(' *** Completed execution of ',A,' ***')
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE STATUS ****
C-----------------------------------------------------------------------
C
      END
