*COMDECK UNTCNV
C***********************************************************************
C     /UNTCNV/ (from FORTRAN statement: INCLUDE 'UNTCNV.INC')
C      BUNITS = BRAGFLO type units (CHARACTER*8)
C             = 'ENGLISH' or
C             = 'SI'
C      FTM    = Conversion from ft to m (REAL)
C      CFM3   = Conversion from ft**3 to m**3 (REAL)
C***********************************************************************
C234567
      DOUBLE PRECISION CFM3, FTM  !apg REAL
      CHARACTER*(8) BUNITS
      COMMON /UNTCNV/ BUNITS, CFM3,FTM
C***********************************************************************


