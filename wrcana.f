*DECK WRCANA
      SUBROUTINE WRCANA( TIME,    HIFLAG,  NAMHIV2,
     $                   NAMGLV2, NAMELV2, VALHIV2, VALGLV2, VALELV2 )
!apg Removed Q,C
C***********************************************************************
C****                                                               ****
C****         W_R_ite  C_AMDAT  A_N_A_lysis  section  routine       ****
C****         - -      -        - - -                               ****
C***********************************************************************
C
C  PURPOSE:     Writes analysis section of CAMDAT for a single timestep.
C
C  AUTHOR:      Jonathan S. Rath
C
C  UPDATED:     26-OCT-1993  --JDS: Cosmetic changes; used in POSTBRAG
C                                   for BRAGFLO version >2.30VV.
C                                   Added args NAMHIV2 and VALHIV2, and
C                                   new section of code to get history
C                                   variables written to CAMDAT file.
C                                   Deleted ISEVOK2 from argument list
C                                   and declarations; not used anymore.
C               05-AUG-1993  --JSR: First Edition for POSTBRAG_T.
C
C  CALLED BY:   PROCBO
C
C  CALLS:       QAABORT
C
C  ARGUMENTS
C  ENTRY/
C    --common blocks
C     /FILEUN/ (from FORTRAN statement: 'fileun.inc')
C      NOUTFL = Device no. of diagnostics/debut file
C      ODB    = Device no. of output CAMDAT file
C
C     /GEOPAR/ (from FORTRAN statement: 'geopar.inc')
C      NVAREL = No. of old Element variables (INTEGER)
C      NVARGL = No. of old GlobaL variables (INTEGER)
C      NVARHI = No. of old History variables (INTEGER)
C      NVARND = No. of old Nodal variables (INTEGER)
C
C     /NEWANA/ (from FORTRAN statement: 'newana.inc')
C      NVAREL2= Number of new CAMDAT Element variables (INTEGER)
C      NVARGL2= Number of new CAMDAT Global variables (INTEGER)
C      NVARHI2= Number of new CAMDAT History variables (INTEGER)
C
C    --through subroutine call
C      HIFLAG = History flag (LOGICAL)
C      TIME   = Time (REAL)
C      NAMGLV2= Names of new GLOBAL variables array (CHAR*8)
C      NAMELV2= "            ELEMENT "
C      NAMHIV2= "            HISTORY "
C      VALGLV2= New GLOBAL  variables values array (REAL)
C      VALELV2= "   ELEMENT "
C      VALHIV2= "   HISTORY "
C
C  LOCAL/
C      none
C
C  EXIT/
C      none
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'newana.inc'
      INTEGER I, IERR, IND
      INTEGER IDUM
      LOGICAL HIFLAG  !apg was REAL
      DOUBLE PRECISION TIME  !apg REAL
      DOUBLE PRECISION VALELV2(*), VALGLV2(*), VALHIV2(*)  !apg REAL
      CHARACTER*(8) NAMELV2(*), NAMGLV2(*), NAMHIV2(*)
      LOGICAL WHOLE
C
C<><><><><><><><><><><>
C...BEGIN PROCEDURES...
C<><><><><><><><><><><>
C
C     ...Write a time step to the CAMDAT
!apg      IF(HIFLAG.EQ.1.0)THEN
!apg         WHOLE = .FALSE.
!apg      ELSEIF(HIFLAG.EQ.0.0)THEN
!apg         WHOLE = .TRUE.
!apg      ENDIF
      WHOLE = .NOT. HIFLAG  !apg
      CALL DBOTIME( ODB, TIME, WHOLE, IERR )

C     ...Write POSTBRAG CAMDAT history variables
      IF(NVARHI2.GT.0)THEN
         DO 110 I=1,NVARHI2
            CALL DBOVAR( ODB, 'HISTORY', NAMHIV2(I), 0,
     $                   VALHIV2(I), IDUM, IDUM, IERR )
            IF(IERR.NE.0)CALL QAABORT('(WRCANA) - Error writing'//
     $      ' NEW HISTORY variable values to CDB ***')
 110     CONTINUE
      ENDIF

C     ...Write POSTBRAG CAMDAT global variables
      IF(NVARGL2.GT.0 .AND. .NOT.HIFLAG)THEN
         DO 210 I=1,NVARGL2
            CALL DBOVAR( ODB, 'GLOBAL', NAMGLV2(I), 0,
     $                   VALGLV2(I), IDUM, IDUM, IERR )
            IF(IERR.NE.0)CALL QAABORT('(WRCANA) - Error writing'//
     $      ' NEW GLOBAL variable values to CDB ***')
 210     CONTINUE
      ENDIF

C     ...Write POSTBRAG CAMDAT element variables
      IF(NVAREL2.GT.0 .AND. .NOT.HIFLAG)THEN
         DO 410 I=1,NVAREL2
            IND = (I-1)*NUMEL+1
            CALL DBOVAR( ODB,         'ELEMENT', NAMELV2(I), 0,
     $                   VALELV2(IND), IDUM, IDUM, IERR )
            IF(IERR.NE.0)CALL QAABORT('(WRCANA) - Error writing'//
     $      ' NEW ELEMENT variable values to CDB ***')
 410     CONTINUE
      ENDIF

C     ...Write the completed time-step and advance to the next step
      CALL DBOSTEP( ODB, IDUM, IDUM, IERR )
      IF(IERR.NE.0)CALL QAABORT('(WRCANA) - Error writing a completed'//
     $' time-step and advancing to next step /DBOSTEP ***')

      RETURN
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE WRCANA *****
C-----------------------------------------------------------------------
C
      END
