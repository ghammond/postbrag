*DECK WRCHEA
      SUBROUTINE WRCHEA( INFO,    FILNAM,  EXINFO,
     $                   INFO2,   IASATR,  NAMATR,  ATTRIB,
     $                   NAMHIV2, NAMGLV2, NAMELV2 )
!apg Removed Q,C and ISEVOK2
C***********************************************************************
C****                                                               ****
C****        W_R_ite   C_amdat  H_E_A_der  section  module          ****
C****        - -       -        - - -                               ****
C***********************************************************************
C
C  PURPOSE:     Writes the header section of the new CAMDAT
C
C  AUTHOR:      Jonathan S. Rath
C
C  UPDATED:     26-OCT-1993  --JSR: Added arguments (INFO, EXINFO,
C                                   INFO2, NAMHIV2 ).
C                                   Added call to DBOINFO.
C                                   Adapted to POSTBRAG for
C                                   BRAGFLO version >2.30VV.
C               05-AUG-1993  --JSR: First Edition for POSTBRAG_T.
C
C  CALLED BY:   PROCBO
C
C  CALLS:       DBOTITLE
C               DBOINFO
C               DBOHEAD
C               DBOATTR
C               DBOVRNAM
C               QAABORT
C
C  ARGUMENTS
C  ENTRY/
C    --common blocks
C     /BRAG/ (from FORTRAN statement: 'brag.inc')
C      NUMFIL = Number of files used in BRAGFLO
C
C     /FILEUN/ (from FORTRAN statement: 'fileun.inc')
C      NOUTFL = Device no. of diagnostics/debut file
C      ODB    = Device no. of output CAMDAT file
C
C     /GEOPAR/ (from FORTRAN statement: 'geopar.inc')
C      NDIM   = Number of physical DIMensions
C      NELBLK = Number of ELement BLocKs in mesh
C      NUMEL  = NUmber of ELeMents in mesh
C      NUQATR = No. of unique attribute names
C      NUQATR2= No. of unique attribute names (w/additional att.)
C      HEADER = Problem description or title (CHARACTER*80)
C      NEXINFO= Number of BRAGFLO "extra" information records (INTEGER)
C             = +1 for BRAGFLO revision date
C               +1 for CPUNAME
C               +1 for BRAGFLO units used
C               +9 for extra file name header records
C               +NVAREL2
C               +NVARGL2
C               +NVARHI2
C             = (12+NVAREL2+NVARGL2+NVARHI2)
C      NXINFO = Number of optional additional information records
C      NXINFO2= Number of additional information records +
C               additional records required by POSTBRAG.
C      UNITSC = Unit system used throught CAMDAT (CHARACTER*8)
C
C     /NEWANA/ (from FORTRAN statement: 'newana.inc')
C      NVAREL2= Number of new CAMDAT Element variables (INTEGER)
C      NVARHI2= Number of new CAMDAT History variables (INTEGER)
C      NVARGL2= Number of new CAMDAT Global variables (INTEGER)
C
C    --through subroutine call
C      IASATR = Attribute existence in material array (LOGICAL)
C      NAMATR = Names of attributes (CHARACTER*8)
C      ATTRIB = Values of attributes (REAL)
C      INFO   = Additional information records array (CHAR*80)
C      EXINFO = BRAGFLO "extra" information records array (CHAR*80)
C      FILNAM = BRAGFLO filenames array (CHAR*80)
C      NAMHIV2= Names of new HISTORY variables array (CHAR*8)
C      NAMGLV2= Names of new GLOBAL  variables array (CHAR*8)
C      NAMELV2= Names of new ELEMENT variables array (CHAR*8)
C
C  LOCAL/
C      none
C
C  EXIT/
C      none
C
C***********************************************************************
C234567
      IMPLICIT NONE
      INCLUDE 'brag.inc'
      INCLUDE 'fileun.inc'
      INCLUDE 'geopar.inc'
      INCLUDE 'newana.inc'
      INTEGER I, IERR, IERR1, I1, J, K, L, M
      INTEGER IDUM
      DOUBLE PRECISION ATTRIB(*)  !apg REAL
      CHARACTER*(8)  NAMATR(*),  NAMELV2(*), NAMGLV2(*), NAMHIV2(*)
      CHARACTER*(80) EXINFO(*), FILNAM(*), INFO(*), INFO2(*)
      LOGICAL IASATR(*)
C
C<><><><><><><><><><><>
C...BEGIN PROCEDURES...
C<><><><><><><><><><><>
C
C     ...Set up the "new" BRAGFLO-CAMDAT information records
C        array (CAMDAT INFO(*) + BRAGFLO FILNAM(*) + EXINFO(*) arrays)
      I1 = 0
      DO 10 I=1,NXINFO
         I1             = I1+1
         INFO2(I1)(:80) = INFO(I)(:80)
  10  CONTINUE
      DO 20 I=1,NUMFIL
         I1             = I1+1
         INFO2(I1)(:80) = FILNAM(I)(:80)
  20  CONTINUE
      DO 30 I=1,NEXINFO
         I1             = I1+1
         INFO2(I1)(:80) = EXINFO(I)(:80)
  30  CONTINUE
      IF(I1.NE.NXINFO2)CALL QAABORT('(WRCHEA) - Error setting'//
     $' up INFO2(*) array: I1.NE.NXINFO2 ??? ***')

C     ...Set the output CAMDAT addtional information data
      CALL DBOINFO( ODB, NXINFO2, INFO2, IERR )
      IF(IERR.GT.0)CALL QAABORT('(WRCHEA) - Error setting '//
     $'additional information data to output CAMDAT ***')

C     ...Set the output CAMDAT problem description title
      CALL DBOTITLE( ODB, HEADER, IERR )
      IF(IERR.GT.0)CALL QAABORT('(WRCHEA) - Error setting '//
     $'title of the output CAMDAT ***')

C     ...WIPE OUT all existing input CAMDAT attributes
      CALL DBOATTR( ODB,'RESET','*',.FALSE.,0.,IERR )
      IF(IERR.NE.0)CALL QAABORT('(WRCHEA) - RESETTING attrib'//
     $'utes to output CAMDAT file ***')

C     ...Write attributes to output CAMDAT file
      DO 40 I=1,NUQATR2
         J=(I-1)*NELBLK+1
         K=(I-1)*NUMEL+1
         CALL DBOATTR(ODB,'ADD',NAMATR(I),IASATR(J),ATTRIB(K),IERR1)
         IERR = IERR+IERR1
  40  CONTINUE
      IF(IERR.NE.0)THEN
         WRITE(NOUTFL,*)' NAMATR   J        IASATR   ATTRIB'
         WRITE(NOUTFL,*)' -------- -------- -------- -----------'
         DO 60 I=1,NUQATR2
            J=(I-1)*NELBLK+1
            K=(I-1)*NUMEL+1
            WRITE(NOUTFL,'(2X,A,1X,I8,1X,L8,1X,E12.3)')
     $      NAMATR(I),J,IASATR(J),ATTRIB(K)
            DO 50 L=1,NELBLK-1
               M = J+L
               WRITE(NOUTFL,'(11X,I8,1X,L8,1X,E12.3)')
     $         M,IASATR(M),ATTRIB(K)
  50        CONTINUE
  60     CONTINUE
         CALL QAABORT('(WRCHEA) - Writing attributes to'//
     $                ' output CAMDAT file ***')
      ENDIF

C     ...Write "HEADER" section of the new CAMDAT
      CALL DBOHEAD( ODB, IDUM, IDUM, IERR )
      IF(IERR.GT.0)CALL QAABORT('(WRCHEA) - Error writing '//
     $'HEADER section to the output CAMDAT ***')

C     ...WIPE OUT all existing input CAMDAT analysis variables:
C        (History, Global, Nodal, and Element)
      CALL DBOVRNAM( ODB,    'HISTORY', 'RESET', 0,     '*',
     $               IDUM, IDUM, IDUM, IERR )
      IF(IERR.NE.0)CALL QAABORT('(WRCHEA) - RESETTING History'//
     $'variables to output CAMDAT file ***')
      CALL DBOVRNAM( ODB,    'GLOBAL',  'RESET', 0,     '*',
     $               IDUM, IDUM, IDUM, IERR )
      IF(IERR.NE.0)CALL QAABORT('(WRCHEA) - RESETTING Global'//
     $'variables to output CAMDAT file ***')
      CALL DBOVRNAM( ODB,    'NODAL',   'RESET', 0,     '*',
     $               IDUM, IDUM, IDUM, IERR )
      IF(IERR.NE.0)CALL QAABORT('(WRCHEA) - RESETTING Nodal'//
     $'variables to output CAMDAT file ***')
      CALL DBOVRNAM( ODB,    'ELEMENT', 'RESET', 0,     '*',
     $               IDUM, IDUM, IDUM, IERR )
      IF(IERR.NE.0)CALL QAABORT('(WRCHEA) - RESETTING Element'//
     $'variables to output CAMDAT file ***')

C     ...Write POSTBRAG CAMDAT history variable names
      IF(NVARHI2.GT.0)THEN
         CALL DBOVRNAM( ODB,    'HISTORY', 'ADD', NVARHI2, NAMHIV2,
     $                  IDUM, IDUM, IDUM, IERR )
         IF(IERR.GT.0)CALL QAABORT('(WRCHEA) - Error writing '//
     $   'new HISTORY variable names to output CDB ***')
      ENDIF

C     ...Write POSTBRAG CAMDAT global variable names
      IF(NVARGL2.GT.0)THEN
         CALL DBOVRNAM( ODB,    'GLOBAL', 'ADD', NVARGL2, NAMGLV2,
     $                  IDUM, IDUM, IDUM, IERR )
         IF(IERR.GT.0)CALL QAABORT('(WRCHEA) - Error writing '//
     $   'new GLOBAL variable names to output CDB ***')
      ENDIF

C     ...Write POSTBRAG CAMDAT element variable names
      IF(NVAREL2.GT.0)THEN
         CALL DBOVRNAM( ODB,    'ELEMENT', 'ADD', NVAREL2, NAMELV2,
     $                  IDUM, IDUM, IDUM, IERR )
         IF(IERR.GT.0)CALL QAABORT('(WRCHEA) - Error writing '//
     $   'new ELEMENT variable names to output CDB ***')
      ENDIF

      RETURN
C
C-----------------------------------------------------------------------
C**** End of SUBROUTINE WRCHEA *****
C-----------------------------------------------------------------------
C
      END
